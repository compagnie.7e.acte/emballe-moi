/////////////////////////////////////
//Pour la partie SERVEUR
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  
  res.sendFile(__dirname + '/index.html');
});

io.sockets.on('connection', function (socket) {
		
    // Quand un client se connecte, on lui envoie un message
    socket.broadcast.emit('message', {message: "vous êtes bien connecté"});

    console.log("Nouveau client connecté au serveur");
  

  	//On réception le FLAG &emitVibration, du coup on emmet la vibration à tous les clients
  	socket.on('&emitVibration', function() {
    
    	console.log("je recois un emit de vibration");

        // On signale aux autres clients qu'il y a un nouveau venu
    	socket.broadcast.emit('&vibration', {message: "1"});

    	console.log('vibration emise');
    });

    //Lorsque l'évéènement &emitUrl est émit depuis un client
    //status = identifiant de la page
    socket.on('&emitUrl', function(url) {

      console.log(url);
      socket.broadcast.emit("&url", {url : url});

    });
});


http.listen(3000, function(){
  console.log('listening on *:3000');

});
