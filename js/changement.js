    (function($) {
      "use strict";

      $(function() {
        var canVibrate = "vibrate" in navigator || "mozVibrate" in navigator;

        if (canVibrate && !("vibrate" in navigator))
          navigator.vibrate = navigator.mozVibrate;

        /////////////////////////////////////
        //Pour la partie USER (côté client)
        var socket = io.connect('http://172.16.100.166:3000');

       //Lorsque que le message &vibration (qui est le flag) est catché dans la réception d'un socket
        socket.on('&vibration', function(response) {
          //On emmet la vibration
          navigator.vibrate(10000);
        });

        socket.on("&url", function(response) {

          var url = response.url;
          document.location.href = url.url;
          
        });
      });
    })(jQuery);