<?php  
  require_once '../includes/config.php';
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  
  <title>Emballe Moi | C.G.U</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">
  <!-- Jquery Lib -->
  <script src="../js/jquery-3.3.1.min.js"></script>
  <!-- Socket.io Lib -->
  <script src="../socket.io/socket.io.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="../index2.html"><b>Emballe </b>Moi</a>
  </div>

  <div class="register-box-body">
    <font color="white"><p><b>EmballeMoi</b> est une application totalement gratuite, sans publicité ni collecte ou revente de données personnelles.</p>

    <p>Son financement repose entièrement sur l’activité de l’Association Compagnie 7ème Acte.</p>

    <p>Seul les prestations, dons, mécénats assurent et pérennisent nos recherches et notre développement. 
        A l'issue du spectacle "Emballe Moi" vous pourrez conserver votre avatar et les statistiques relatives aux réponses lors de la représentation sous forme PDF. </p>

    <p>Vous pouvez visiter le site de l'association "http://www.compagnie7emeacte.fr/" ou nous contacter par mail à <a href="mailto:compagnie7emeacte@orange.fr">compagnie7emeacte@orange.fr</a></p></font>

    <a href="profile.php" class="btn btn-danger btn-block btn-flat">J'ai compris !</a>
  </div>
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<script src="../js/changement.js"></script>
</body>
</html>
