<?php
	// Connexion à la BDD
	require_once '../includes/config.php';

      $req = $odb -> prepare('SELECT * FROM questions WHERE visible = 1');
      $req -> execute();
      $question = $req -> fetch();

      $countResultat = $odb->prepare('SELECT COUNT(*) FROM resultats WHERE idquestion=:idquestion AND iduser=:iduser');
      $countResultat->execute(array('idquestion' => $question['id'], 'iduser' => $_SESSION['idmembre']));
      $nbResultat = $countResultat->fetchColumn(0);

    if ($nbResultat == 0) {

      $tempsActuel = time();
      $tempsRestant = $question['temps']-$tempsActuel+30;

      if($tempsRestant>0){
        echo 'Il te reste ' . $tempsRestant . ' secondes pour répondre à la question.<br />';
      }

      else{
        echo 'Dépêche-toi !';
      }
    }
?>
