<?php

header ("Content-type: image/png"); // L'image que l'on va créer est un jpeg

  // On charge d'abord les images
  $image = imagecreatefrompng("../dist/img/emballe_moi.png"); // Image Cheveux
  $blanc = imagecolorallocate($image, 255, 255, 255);
  $police = 5;
  $x = 40;
  $y = 130;
  $texte_a_ecrire = date("d/m/y");  
  imagestring($image, $police, $x, $y, $texte_a_ecrire, $blanc);



// On enregistre l'image de destination qui a été fusionnée
imagepng($image, "../dist/img/emballe_moi_date.png");
// On affiche l'image de destination qui a été fusionnée
imagepng($image);
?>