<?php

  require_once '../includes/config.php';
  
  $query = $odb->prepare("SELECT `actif` FROM `config`");
  $query->execute();
  $actif = $query -> fetchColumn(0);

  if ($actif['actif'] == 0) {
    header('Location: attente.php');
  }
?>

<?php
    $derniermessage = $odb->prepare('SELECT * FROM `EMBALLE_MESSAGES` WHERE visible=1 ORDER BY `idmessage` DESC');
    $derniermessage->execute();
    $infosderniersmessage = $derniermessage->fetch();

    if($infosderniersmessage['conversation'] == "potesacha")
    {
      echo '<center><h1>Pote <i class="fa fa-circle text-success" style="font-size:20px; color:#78e29f;"></i></h1></center>';
    }

    else if($infosderniersmessage['conversation'] == "camillesacha")
    {
      echo '<center><h1>Camille <i class="fa fa-circle text-success" style="font-size:20px; color:green;"></i></h1></center>';
    }

    else if($infosderniersmessage['conversation'] == "hugosacha")
    {
      echo '<center><h1>Hugo <i class="fa fa-circle text-success" style="font-size:20px; color:green;"></i></h1></center>';
    }
  ?>

  <h6><em>Aujourd'hui</em> <?php echo date("H:i"); ?></h6>
    <ul class="chat">
      <?php
        // On recuère tous les messages visible de type "affichage"
        $selectmessage = $odb->prepare('SELECT * FROM EMBALLE_MESSAGES WHERE visible=1 AND type="affichage" AND conversation=:conversation ORDER BY idmessage');
        $selectmessage->execute(array('conversation' => $infosderniersmessage['conversation']));

        while ($infosmessage = $selectmessage->fetch())
        {
          // Si le message provient de l'autre personne
          if($infosmessage['idmembre'] == 1)
          {
      ?>
            <li class="them">
              <?php
                if($infosderniersmessage['conversation'] == "potesacha")
                {
                  echo '<p>Pote</p>';
                }

                else if($infosderniersmessage['conversation'] == "camillesacha")
                {
                  echo '<p>Camille</p>';
                }

                else if($infosderniersmessage['conversation'] == "hugosacha")
                {
                  echo '<p>Hugo</p>';
                }
              ?>
              <img src="img/<?php echo $infosderniersmessage['conversation']; ?>.png" style="border-radius: 50%; width:50px; border-width:1px; border-style:solid; border-color:black;" />
              
              <?php
                if($infosderniersmessage['conversation'] == "potesacha")
                {
              ?>
                  <blockquote style="background-color: #5bcc85;"><?php echo $infosmessage['message']; ?></blockquote>
              <?php
                }

                else if($infosderniersmessage['conversation'] == "camillesacha")
                {
              ?>
                  <blockquote style="background-color: #e296ea;"><?php echo $infosmessage['message']; ?></blockquote>
              <?php
                }

                else if($infosderniersmessage['conversation'] == "hugosacha")
                {
              ?>
                  <blockquote style="background-color: #ead296;"><?php echo $infosmessage['message']; ?></blockquote>
              <?php
                }
              ?>
            </li>
      <?php
          }

          // Si le message provient de nous
          if($infosmessage['idmembre'] == 2)
          {
      ?>
            <li class="me">
              <blockquote><?php echo $infosmessage['message']; ?></blockquote>
              <img src="img/sacha.png" style="border-radius: 50%; width:50px; border-width:1px; border-style:solid; border-color:black;" />
              <br />
              <em>Sacha</em>
            </li>
        <?php
            // On rend invisible le message précédant (quand il est de type "ecriture" et qu'il est visible)
            $updateecriture = $odb->prepare('UPDATE EMBALLE_MESSAGES SET visible=2 WHERE type="ecriture" AND visible=1 AND idmessage=:idmessage-1 AND conversation=:conversation LIMIT 1');
            $updateecriture->execute(array('idmessage' => $infosmessage['idmessage'], 'conversation' => $infosderniersmessage['conversation']));
          }
        }
      ?>

      <li class="typing">
        <blockquote>&bull; &bull; &bull;</blockquote>
      </li>

      <i class="fa fa-mobile" aria-hidden="true"></i> Envoyé depuis messenger

      <?php
        // On récupère le message de type "ecriture" et qui est visible
        $selectecriture = $odb->prepare('SELECT * FROM EMBALLE_MESSAGES WHERE visible=1 AND type="ecriture" AND conversation=:conversation ORDER BY idmessage LIMIT 1');
        $selectecriture->execute(array('conversation' => $infosderniersmessage['conversation']));
        $infosecriture = $selectecriture->fetch();
      ?>

      <h3><?php echo $infosecriture['message']; ?><span>&nbsp;</span></h3>
      
    </ul>