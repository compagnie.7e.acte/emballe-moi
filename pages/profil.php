<?php 
  
  require_once '../includes/config.php';

  // On récupère le status du spectacle
  $query = $odb->prepare("SELECT `actif` FROM `config`");
  $query->execute();
  $actif = $query -> fetchColumn(0);

  // Si le spectacle n'est pas "ouvert", on redirige l'utilisateur vers la page d'attente
  if ($actif['actif'] == 0) {
    header('Location: attente.php');
  }

  //Si le SESSION de le profil est déjà créer, on redirige l'utilisateur vers la page d'avatar.
  if (isset($_SESSION['sexe'])) {
    header('Location: avatar.php');
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Emballe Moi | Profil</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">
  <!-- Jquery Lib -->
  <script src="../js/jquery-3.3.1.min.js"></script>
  <!-- Socket.io Lib -->
  <script src="../socket.io/socket.io.js"></script>

  <style>
    p
    {
      color:#fff;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <img src="image_date.php">
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Profil</p>
                <?php

                // Au clique sur le bouton "inscription"
                if (isset($_POST['nextBtn']))
                {

                    // On recupere les POST form
                        $username = $_POST['username'];
                        $sexe     = $_POST['sexe'];
                        $email    = $_POST['email'];
                        if (empty($_POST['age']))
                        {
                          $age = 0;
                        }
                        else
                        {
                          $age = $_POST['age'];
                        }

                        $errors   = array();

                        if ($username != NULL)
                        {
                          // On fait les différentes vérifications
                          $checkUsername = $odb -> prepare("SELECT * FROM `users` WHERE `username`= :username");
                          $checkUsername -> execute(array(':username' => $username));
                          $countUsername = $checkUsername -> rowCount();

                          if ($countUsername != 0)
                          {
                            $errors[] = 'Ce nom d\'utilisateur est déjà utilisé';
                          }
                          
                        }

                        if ($email != NULL) {

                          $checkEmail = $odb -> prepare("SELECT * FROM `users` WHERE `email`= :email");
                          $checkEmail -> execute(array(':email' => $email));
                          $countEmail = $checkEmail -> rowCount();

                          if ($countEmail != 0)
                          {
                              $errors[] = 'Cette adresse e-mail est déjà utilisée';
                          }

                        }

                        // S'il n'y a pas d'erreurs, alors on insère l'utilisateur dans la base de donnée
                        if (empty($errors))
                        {
                          $selectUser = $odb->prepare('SELECT COUNT(*) FROM users');
                          $selectUser->execute();
                          $nbUser = $selectUser->fetchColumn(0);

                          $_SESSION['idmembre'] = $nbUser+1;
                          $_SESSION['username'] = $username;
                          $_SESSION['sexe']   = $sexe;
                          $insertUser = $odb -> prepare("INSERT INTO `users` VALUES(:id, :username, :sexe, :email, :age)");
                          $insertUser -> execute(array('id' => $_SESSION['idmembre'], ':username' => $username, ':sexe' => $sexe, ':email' => $email, ':age' => $age));
                          //Send mail here
                          
                          echo '<div class="alert alert-success fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Bravo!</strong> Ton profil à été enregistré.. Merci de patienter...<div>';
                        }
                        
                        // sinon, on affiche les erreurs
                        else
                        {
                            echo '<div class="alert alert-block alert-danger fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Oops!</strong><br />';
                            foreach($errors as $error)
                            {
                                echo '- '.$error.'<br />';
                            }
                            echo '</div>';
                        }
                    }

                    if (!(isset($_SESSION['username'])))
                    {
            ?>
    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Pseudo" name="username" required="" autofocus>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <select class="form-control" name="sexe" id="sexe">
          <option value="0">Homme</option>
          <option value="1">Femme</option>
        </select>
      </div>
      <div class="form-group has-feedback">
        <input type="number" name="age" class="form-control" placeholder="Âge" min="0">
        <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" name="email" class="form-control" placeholder="E-mail">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" required=""> J'accepte les <a href="cgu.php">conditions d'utilisations</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat" name="nextBtn">Suivant</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <?php
      }
    ?>
</div>
<!-- /.register-box -->

<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<script src="../js/changement.js"></script>
</body>
</html>
