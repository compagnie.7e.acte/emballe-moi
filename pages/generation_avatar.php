<?php

session_start();

if($_SESSION['sexe'] == 0)
    $sexe = "M";

  else
    $sexe = "F";

header ("Content-type: image/png"); // L'image que l'on va créer est un jpeg

  // On charge d'abord les images
  $visage = imagecreatefrompng("../dist/img/Visage" . $sexe . "/" . $_SESSION['visage']); // Image Visage
  $cheveux = imagecreatefrompng("../dist/img/Cheveux" . $sexe . "/". $_SESSION['cheveux']); // Image Cheveux
  $yeux = imagecreatefrompng("../dist/img/Yeux" . $sexe . "/" . $_SESSION['yeux']); // Image Yeux
  $nez = imagecreatefrompng("../dist/img/Nez" . $sexe . "/" . $_SESSION['nez']);
  $bouche = imagecreatefrompng("../dist/img/Bouche" . $sexe . "/" . $_SESSION['bouche']);
  $accessoire = imagecreatefrompng("../dist/img/Accessoires" . $sexe . "/" . $_SESSION['accessoire']);


  // Les fonctions imagesx et imagesy renvoient la largeur et la hauteur d'une image
$largeur_cheveux = imagesx($cheveux);
$hauteur_cheveux = imagesy($cheveux);

$largeur_visage = imagesx($visage);
$hauteur_visage = imagesy($visage);

$largeur_yeux = imagesx($yeux);
$hauteur_yeux = imagesy($yeux);

$largeur_nez = imagesx($nez);
$hauteur_nez = imagesy($nez);

$largeur_bouche = imagesx($bouche);
$hauteur_bouche = imagesy($bouche);

$largeur_accessoire = imagesx($accessoire);
$hauteur_accessoire = imagesx($accessoire);

// On met le logo (source) dans l'image de destination (la photo)
imagecopy($visage, $yeux, 0, 0, 0, 0, $largeur_yeux, $hauteur_yeux);
imagecopy($visage, $cheveux, 0, 0, 0, 0, $largeur_cheveux, $hauteur_cheveux);
imagecopy($visage, $nez, 0, 0, 0, 0, $largeur_nez, $hauteur_nez);
imagecopy($visage, $bouche, 0, 0, 0, 0, $largeur_bouche, $hauteur_bouche);
imagecopy($visage, $accessoire, 0, 0, 0, 0, $largeur_accessoire, $hauteur_accessoire);



// On affiche l'image de destination qui a été fusionnée
imagepng($visage);

// On enregistre l'image
imagepng($visage, "../dist/img/avatars/avatar" . $_SESSION['idmembre'] . ".png");

?>