<?php

  require_once '../includes/config.php';
  
  $query = $odb->prepare("SELECT `actif` FROM `config`");
  $query->execute();
  $actif = $query -> fetchColumn(0);

  if ($actif['actif'] == 0) {
    header('Location: attente.php');
  }
?>


<!DOCTYPE html>
<html>
<head>
  <title>Messages</title>
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <link href="../dist/css/emoji.css" rel="stylesheet">

  <style type="text/css">
    @import url(https://fonts.googleapis.com/css?family=Open+Sans:400+700);

    .message
    {
      overflow-x: scroll;
    }

    .test {
      border-width: 0 10px;
      border-color: black;
      border-style: solid;
      height: 50px;
      border-radius: 10px/50%;
    }

    body { 
      font-family: "Open Sans", sanserif;
      margin: 2em;
    }

    .chat blockquote {
      line-height: 1.7;
      max-width: 75%;
      margin-bottom: 0.5ex;
      display: inline-block;
      padding: 0.5ex 1em;
      border-radius: 1.5ex/1em;
      position: relative;
      box-shadow: 0 15px 1px -12px rgba(0,0,0,0.05);
      font-size: 26px;
    }

    .chat .them, .chat .me { margin-bottom: 1ex; }
    .chat .them { text-align: left; }
    .chat .me { text-align: right; }

    /*<blockquote:last-child::before {
      display:block;
      position: absolute;
      z-index: -999;
      border: 1ex solid transparent;
      bottom: 0;
      border-radius: 1em/1ex;  
    }*/

    .chat .them blockquote { background-color: #ccc; }
    .chat .them blockquote:last-child::before {
      border-bottom-color: #ccc;
      border-right-color: #ccc;
      left: -0.8ex;
      content: "";
    }

    .chat .typing blockquote { 
      background-color: #ccc;
      font-weight: bold;
      color: #666;
    }
    .chat .typing blockquote:last-child::before {
      border-color: #ccc;
      border-width: 0.6ex;
      border-radius: 0.6ex;
      left: -0.2ex;
      box-shadow: -1.1ex 0.3ex 0 -0.3ex #ccc;
      content: "";
    }


    .chat .me blockquote {
      background-color: #007AFF;
      background-image: linear-gradient(180deg,#007AFF,#1D62F0);
      color: white;
    }

    .chat .me blockquote:last-child::before {
      content: "";
      right: -0.8ex;
      border-bottom-color: #1D62F0;  
      border-left-color: #1D62F0;
    }

    h6 {
      font-size: .7em;
      text-align: center;
      margin: 1em 0;
      color: #ccc;
    }

    h6 em { font-weight: bold; }

    ul
    {
      list-style-type:none;
    }


      @-webkit-keyframes typing {
        from { width: 100% }
        to { width:0 }
      }

      @-webkit-keyframes blink-caret {
        from, to { border-color: transparent }
        50% { border-color: black }
      }

      @-moz-keyframes typing {
        from { width: 100% }
        to { width:0 }
      }

      @-moz-keyframes blink-caret {
        from, to { border-color: transparent }
        50% { border-color: black }
      }


    h3 { 
        position: relative; 
        float: left;
        font-size:150%;
      }

      h3 span {
        position:absolute;
        top:0;
        right:0;
        width:0;
        background: white; /* same as background */
        border-left: .1em solid black;
        
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        
        -webkit-animation: typing 2s steps(30, end), /* # of steps = # of characters */
          blink-caret 1s step-end infinite;
        -moz-animation: typing 2s steps(30, end), /* # of steps = # of characters */
          blink-caret 1s step-end infinite;
      }
  </style>
  <!-- Jquery Lib -->
  <script src="../js/jquery-3.3.1.min.js"></script>
  <!-- Socket.io Lib -->
  <script src="../socket.io/socket.io.js"></script>
</head>

<body>
  <div id="messages">
  <?php
    $derniermessage = $odb->prepare('SELECT * FROM `EMBALLE_MESSAGES` WHERE visible=1 ORDER BY `idmessage` DESC');
    $derniermessage->execute();
    $infosderniersmessage = $derniermessage->fetch();

    if($infosderniersmessage['conversation'] == "potesacha")
    {
      echo '<center><h1>Pote <i class="fa fa-circle text-success" style="font-size:20px; color:green;"></i></h1></center>';
    }

    else if($infosderniersmessage['conversation'] == "camillesacha")
    {
      echo '<center><h1>Camille <i class="fa fa-circle text-success" style="font-size:20px; color:green;"></i></h1></center>';
    }

    else if($infosderniersmessage['conversation'] == "hugosacha")
    {
      echo '<center><h1>Hugo <i class="fa fa-circle text-success" style="font-size:20px; color:green;"></i></h1></center>';
    }
  ?>

  
  <h6><em>Aujourd'hui</em> <?php echo date("H:i"); ?></h6>
    <ul class="chat">
      <?php
        // On recuère tous les messages visible de type "affichage"
        $selectmessage = $odb->prepare('SELECT * FROM EMBALLE_MESSAGES WHERE visible=1 AND type="affichage" AND conversation=:conversation ORDER BY idmessage');
        $selectmessage->execute(array('conversation' => $infosderniersmessage['conversation']));

        while ($infosmessage = $selectmessage->fetch())
        {
          // Si le message provient de l'autre personne
          if($infosmessage['idmembre'] == 1)
          {
      ?>
            <li class="them">
              <?php
                if($infosderniersmessage['conversation'] == "potesacha")
                {
                  echo '<p>Pote</p>';
                }

                else if($infosderniersmessage['conversation'] == "camillesacha")
                {
                  echo '<p>Camille</p>';
                }

                else if($infosderniersmessage['conversation'] == "hugosacha")
                {
                  echo '<p>Hugo</p>';
                }
              ?>
              <img src="img/<?php echo $infosderniersmessage['conversation']; ?>.png" style="border-radius: 50%; width:50px; border-width:1px; border-style:solid; border-color:black;" />
              
              <?php
                if($infosderniersmessage['conversation'] == "potesacha")
                {
              ?>
                  <blockquote style="background-color: #5bcc85;"><?php echo $infosmessage['message']; ?></blockquote>
              <?php
                }

                else if($infosderniersmessage['conversation'] == "camillesacha")
                {
              ?>
                  <blockquote style="background-color: #e296ea;"><?php echo $infosmessage['message']; ?></blockquote>
              <?php
                }

                else if($infosderniersmessage['conversation'] == "hugosacha")
                {
              ?>
                  <blockquote style="background-color: #ead296;"><?php echo $infosmessage['message']; ?></blockquote>
              <?php
                }
              ?>
            </li>
      <?php
          }

          // Si le message provient de nous
          if($infosmessage['idmembre'] == 2)
          {
      ?>
            <li class="me">
              <blockquote><?php echo $infosmessage['message']; ?></blockquote>
              <img src="img/sacha.png" style="border-radius: 50%; width:50px; border-width:1px; border-style:solid; border-color:black;" />
              <br />
              <em>Sacha</em>
            </li>
        <?php
            // On rend invisible le message précédant (quand il est de type "ecriture" et qu'il est visible)
            $updateecriture = $odb->prepare('UPDATE EMBALLE_MESSAGES SET visible=2 WHERE type="ecriture" AND visible=1 AND idmessage=:idmessage-1 AND conversation=:conversation LIMIT 1');
            $updateecriture->execute(array('idmessage' => $infosmessage['idmessage'], 'conversation' => $infosderniersmessage['conversation']));
          }
        }
      ?>

      <li class="typing">
        <blockquote>&bull; &bull; &bull;</blockquote>
      </li>

      <i class="fa fa-mobile" aria-hidden="true"></i> Envoyé depuis messenger

      <?php
        // On récupère le message de type "ecriture" et qui est visible
        $selectecriture = $odb->prepare('SELECT * FROM EMBALLE_MESSAGES WHERE visible=1 AND type="ecriture" AND conversation=:conversation ORDER BY idmessage LIMIT 1');
        $selectecriture->execute(array('conversation' => $infosderniersmessage['conversation']));
        $infosecriture = $selectecriture->fetch();
      ?>

      <h3><?php echo $infosecriture['message']; ?><span>&nbsp;</span></h3>


      <?php
        // On vérifie que le message est visible
        /*$countVisible = $odb->prepare('SELECT COUNT(*) FROM EMBALLE_MESSAGES WHERE visible=1 AND idmessage=:idmessage AND conversation="potesacha"');
        $countVisible->execute(array('idmessage' => $infosecriture['idmessage']));
        $nbVisible = $countVisible->fetchColumn(0);

        // Si il est visible
        if($nbVisible == 1)
        {
          // On rend invisible le message écrit
          $updateecriture = $odb->prepare('UPDATE EMBALLE_MESSAGES SET visible=2 WHERE idmessage=:idmessage AND conversation="potesacha"');
          $updateecriture->execute(array('idmessage' => $infosecriture['idmessage']));

          // On rend visible le prochain message
          $updatemessage = $odb->prepare('UPDATE EMBALLE_MESSAGES SET visible=1 WHERE visible=0 AND conversation="potesacha" LIMIT 1');
          $updatemessage->execute();
        }*/
      ?>
    </ul>
  </div>

  <div id="bottom"></div>

  <script>
    setInterval('load_messages()', 6000);
    function load_messages() {
      $('#messages').load('load_messages.php');
    }
  </script>
  
  <script type="text/javascript">
    function bottom() {
      document.getElementById( 'bottom' ).scrollIntoView();
      window.setTimeout( function () { top(); }, 2000 );
    };

    bottom();
  </script>
  <script src="../js/changement.js"></script>
</body>
</html>