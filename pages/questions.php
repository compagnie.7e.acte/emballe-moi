<?php 
  
  require_once '../includes/config.php';

  // On récupère le status du spectacle
  $query = $odb->prepare("SELECT `actif` FROM `config`");
  $query->execute();
  $actif = $query -> fetchColumn(0);

  // Si le spectacle n'est pas "ouvert", on redirige l'utilisateur vers la page d'attente
  if ($actif['actif'] == 0) {
    header('Location: attente.php');
  }

  // Si le SESSION de le profil n'est pas créer, on redirige l'utilisateur vers la page de profil.
  if (!(isset($_SESSION['idmembre']))) {
    header('Location: profile.php');
  }

  if (isset($_POST['sendBtn'])) {

    if (empty($_POST['categ'])) {
      $errors = '<p>Merci de choisir une réponse</p>';
    }


    $resultats = $_POST['categ'];
    $iduser = $_SESSION['idmembre'];
    $idquestion = $_POST['idquestion'];

    $countResultat = $odb->prepare('SELECT COUNT(*) FROM resultats WHERE idquestion=:idquestion AND iduser=:iduser');
    $countResultat->execute(array('idquestion' => $idquestion, 'iduser' => $_SESSION['idmembre']));
    $nbResultat = $countResultat->fetchColumn(0);

    if($nbResultat == 0)
    {
      foreach ($resultats as $resultat)
      {
        $req = $odb -> prepare('INSERT INTO resultats(idquestion, idreponse, iduser) VALUES (:idquestion, :idreponse, :iduser)');
        $req -> execute(array(':idquestion' => $idquestion, ':idreponse' => $resultat, ':iduser' => $iduser));
      }
      
    }

    else
    {
      $errors = '<p>Vous avez déjà répondu a la question !</p>';
    }
    
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Emballe Moi | Questions</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTEE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <!-- Jquery Lib -->
  <script src="../js/jquery-3.3.1.min.js"></script>
  <!-- Socket.io Lib -->
  <script src="../socket.io/socket.io.js"></script>

  <script>
    $(function () {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '5%' // optional
      });
    });
  </script>

  <style>
    p
    {
      color: #fff;
    }
    h4
    {
      color: #fff;
      text-align: center;
    }
    label
    {
      color: #fff;
    }

  </style>
  <style>

  body {
    background-color: #00FF00;
    animation: color-change 15s linear 2s;
    animation-fill-mode: forwards;  
  }

  @keyframes color-change {
    0% { background: #00FF00; }
    33% { background: #ff9900; }
    66% { background: #cc3300; }
    100% { background: #cc3300; }
  }
  </style>
  <script type="text/javascript">

    var count = 16;
     
    function countDown(){
        var timer = document.getElementById("timer");
        if(count > 0){
            count--;
            timer.innerHTML = "Il te reste " + count + " secondes pour répondre a la question.";
            setTimeout("countDown()", 1000);
            timer.style.color = '#FFF';
        }else{
            timer.innerHTML = "Dépêche toi !!";
        }
    }

  </script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="profile.php"><b>Emballe </b>Moi</a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg"><u>Question.. Réponds dès que possible !</u></p>
    <form action="" method="post">
      <div id="questions">
    <?php
      $req = $odb -> prepare('SELECT * FROM questions WHERE visible = 1');
      $req -> execute();
      $question = $req -> fetch();

      $countResultat = $odb->prepare('SELECT COUNT(*) FROM resultats WHERE idquestion=:idquestion AND iduser=:iduser');
      $countResultat->execute(array('idquestion' => $question['id'], 'iduser' => $_SESSION['idmembre']));
      $nbResultat = $countResultat->fetchColumn(0);

      if($nbResultat == 0)
      {
    ?>
          <div class="row">
            <div class="col-xs-8">
              
            <input type="hidden" name="idquestion" value="<?php echo $question['id']; ?>">
            <p><h4><?php echo $question['question']; ?></h4></p>

            <?php

              $req = $odb -> prepare('SELECT * FROM reponses WHERE idquestion = :question');
              $req -> execute(array('question' => $question['id']));

              while ($reponse = $req -> fetch())
              {
                // Si la question est de choix simple
                if($question['type'] == "simple")
                {
              ?>
                  <div class="radio">
                    <label><input type="radio" value="<?php echo $reponse['id']; ?>" name="categ[]"> <?php echo $reponse['reponse']; ?></label>
                  </div>
              <?php
                }

                // Si la question est de choix multiple
                else
                {
              ?>
                  <div class="form-check">
                    <label><input name="categ[]" type="checkbox" class="form-check-input" value="<?php echo $reponse['id']; ?>"> <?php echo $reponse['reponse']; ?></label>
                  </div>
              <?php
                }
              }

            ?>
            </div>
            <!-- /.col -->
            <div class="col-lg">
              <button type="submit" class="btn btn-primary btn-block btn-flat" name="sendBtn">Valider</button>
            </div>
            <!-- /.col -->
          </div>
        
    <?php
      }

      else
      {
        echo '<center><p>Merci.</p></center>';
      }
    ?>
      </div>
    </form>
  </div>
  <!-- /.register-box -->
  <div id="timer" class="register-logo">
    <?php

      $req = $odb -> prepare('SELECT * FROM questions WHERE visible = 1');
      $req -> execute();
      $question = $req -> fetch();

      $countResultat = $odb->prepare('SELECT COUNT(*) FROM resultats WHERE idquestion=:idquestion AND iduser=:iduser');
      $countResultat->execute(array('idquestion' => $question['id'], 'iduser' => $_SESSION['idmembre']));
      $nbResultat = $countResultat->fetchColumn(0);

    if ($nbResultat == 0) {

      $tempsActuel = time();
      $tempsRestant = $question['temps']-$tempsActuel+30;

      if($tempsRestant>0){
        echo 'Il te reste ' . $tempsRestant . ' secondes pour répondre à la question.<br />';
      }

      else{
        echo 'Dépêche-toi !';
      }
    }

    ?>
  </div>

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script src="../js/changement.js"></script>

<script>
  setInterval('load_timer()', 500);
  function load_timer() {
    $('#timer').load('load_timer.php');
  }
</script>
</body>
</html>
