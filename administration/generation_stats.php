<?php
	require_once("../includes/config.php");
	require_once("../jpgraph-4.2.0/src/jpgraph.php");
	require_once("../jpgraph-4.2.0/src/jpgraph_pie.php");

	$data = array();
	$tableauReponses = array();

	$compteur = 0;

	$selectQuestions = $odb->prepare('SELECT * FROM resultats INNER JOIN questions ON resultats.idquestion=questions.id GROUP BY idquestion');
	$selectQuestions->execute();

	while($infosQuestions = $selectQuestions->fetch())
	{
		$countQuestions = $odb->prepare('SELECT COUNT(*) FROM resultats WHERE idquestion=:idquestion');
		$countQuestions->execute(array('idquestion' => $infosQuestions['idquestion']));
		$nbQuestion = $countQuestions->fetchColumn(0);
	
		$selectReponses = $odb->prepare('SELECT * FROM resultats INNER JOIN reponses ON resultats.idreponse=reponses.id WHERE resultats.idquestion=:idquestion GROUP BY idreponse');
		$selectReponses->execute(array('idquestion' => $infosQuestions['idquestion']));
		while($infosReponses = $selectReponses->fetch())
		{
			$countResultats = $odb->prepare('SELECT COUNT(*) FROM resultats INNER JOIN reponses ON resultats.idreponse=reponses.id WHERE resultats.idreponse=:idreponse');
			$countResultats->execute(array('idreponse' => $infosReponses['idreponse']));
			$nbResultats = $countResultats->fetchColumn(0);

			array_push($tableauReponses, $infosReponses['reponse']);

			$pourcentage = round($nbResultats/$nbQuestion*100);
			array_push($data, $pourcentage);
		}

		// On spécifie la largeur et la hauteur du graphique conteneur&#160;
		$graph = new PieGraph(400,300);

		// Titre du graphique
		$graph->title->Set($infosQuestions['question']);

		// Créer un graphique secteur (classe PiePlot)
		$oPie = new PiePlot($data);

		// Légendes qui accompagnent chaque secteur, ici chaque année
		$oPie->SetLegends($tableauReponses);

		// position du graphique (légèrement à droite)
		$oPie->SetCenter(0.4); 

		$oPie->SetValueType(PIE_VALUE_ABS);

		// Format des valeurs de type entier
		$oPie->value->SetFormat('%d&#37;');

		// Ajouter au graphique le graphique secteur
		$graph->Add($oPie);

		// Provoquer l'affichage (renvoie directement l'image au navigateur)
		$graph->Stroke("imagesstats/image" . $compteur . ".png");

		unset($data);
		$data = array();
		unset($tableauReponses);
		$tableauReponses = array();

		$compteur = $compteur+1;
	}
?>