<?php 
  
  require_once '../includes/config.php';

  if (!(isset($_SESSION['admin']))) {
    header('Location: connexion.php');
  }

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Emballe Moi | Vivrations</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">
  <!-- Jquery Lib -->
  <script src="../js/jquery-3.3.1.min.js"></script>
  <!-- Socket.io Lib -->
  <script src="../socket.io/socket.io.js"></script>
  <!-- Bootstrap -->
  <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">

  <div class="register-logo">
    <a href="profile.php"><b>Emballe </b>Moi</a>
  </div>

  <div class="register-box-body">
    <a href="javascript::void(0);" title="Emettre la vibration" class="btn btn-warning btn-block btn-flat btn-vibration">Vibrations</a>
    <a href="gestion.php" class="btn btn-default btn-block btn-flat">Retour</a>
  </div>

</div>

<script>
    (function($) {

        "use strict";

        $(function() {
            /////////////////////////////////////
            //Pour la partie USER (côté client)
            var socket = io.connect('http://172.16.100.166:3000');
            console.log(socket);

            // Au clic sur le bouton de vibration
            $('.btn-vibration').on('click', function() {

                // On créer le flag "emitVibration" avec la valeur 1
                socket.emit('&emitVibration', {value: 1});
                console.log("emitVibration");

                return false;
            });
        });
    })(jQuery);
</script>

</body>
</html>
