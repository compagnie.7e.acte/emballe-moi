<?php 
  session_start();

  require_once '../includes/config.php';

  if (!(isset($_SESSION['admin']))) {
    header('Location: connexion.php');
  }

  if(isset($_GET['suivantpotesacha']))
  {
    // On compte le nombre de message écrit
    $countEcriture = $odb->prepare('SELECT COUNT(*) FROM EMBALLE_MESSAGES WHERE visible=1 AND type="ecriture" AND conversation="potesacha"');
    $countEcriture->execute();
    $nbEcriture = $countEcriture->fetchColumn(0);

    if($nbEcriture == 1)
    {
      // On récupère le message ecriture
      //$selectecriture = $odb->prepare('SELECT * FROM EMBALLE_MESSAGES WHERE visible=1 AND type="ecriture"');
      //$selectecriture->execute();
      //$infosecriture = $selectecriture->fetch();

      // On met à jour le message visible
      $updateecriture = $odb->prepare('UPDATE EMBALLE_MESSAGES SET visible=2 WHERE visible=1 AND type="ecriture" AND conversation="potesacha"');
      $updateecriture->execute();
    }

    // On rend visible le prochain message dans la odb
    $updatevisible = $odb->prepare('UPDATE EMBALLE_MESSAGES SET visible=1 WHERE visible=0 AND conversation="potesacha" LIMIT 1');
    $updatevisible->execute();

    header('Location: gestion_messages.php');
  }

  if(isset($_GET['suivantcamillesacha']))
  {
    // On compte le nombre de message écrit
    $countEcriture = $odb->prepare('SELECT COUNT(*) FROM EMBALLE_MESSAGES WHERE visible=1 AND type="ecriture" AND conversation="camillesacha"');
    $countEcriture->execute();
    $nbEcriture = $countEcriture->fetchColumn(0);

    if($nbEcriture == 1)
    {
      // On récupère le message ecriture
      //$selectecriture = $odb->prepare('SELECT * FROM EMBALLE_MESSAGES WHERE visible=1 AND type="ecriture"');
      //$selectecriture->execute();
      //$infosecriture = $selectecriture->fetch();

      // On met à jour le message visible
      $updateecriture = $odb->prepare('UPDATE EMBALLE_MESSAGES SET visible=2 WHERE visible=1 AND type="ecriture" AND conversation="camillesacha"');
      $updateecriture->execute();
    }

    // On rend visible le prochain message dans la odb
    $updatevisible = $odb->prepare('UPDATE EMBALLE_MESSAGES SET visible=1 WHERE visible=0 AND conversation="camillesacha" LIMIT 1');
    $updatevisible->execute();

    header('Location: gestion_messages.php');
  }

  if(isset($_GET['suivanthugosacha']))
  {
    // On compte le nombre de message écrit
    $countEcriture = $odb->prepare('SELECT COUNT(*) FROM EMBALLE_MESSAGES WHERE visible=1 AND type="ecriture" AND conversation="hugosacha"');
    $countEcriture->execute();
    $nbEcriture = $countEcriture->fetchColumn(0);

    if($nbEcriture == 1)
    {
      // On récupère le message ecriture
      //$selectecriture = $odb->prepare('SELECT * FROM EMBALLE_MESSAGES WHERE visible=1 AND type="ecriture"');
      //$selectecriture->execute();
      //$infosecriture = $selectecriture->fetch();

      // On met à jour le message visible
      $updateecriture = $odb->prepare('UPDATE EMBALLE_MESSAGES SET visible=2 WHERE visible=1 AND type="ecriture" AND conversation="hugosacha"');
      $updateecriture->execute();
    }

    // On rend visible le prochain message dans la odb
    $updatevisible = $odb->prepare('UPDATE EMBALLE_MESSAGES SET visible=1 WHERE visible=0 AND conversation="hugosacha" LIMIT 1');
    $updatevisible->execute();

    header('Location: gestion_messages.php');
  }

  // On rend invisible tous les messages
  if(isset($_GET['razpotesasha']))
  {
    $updatevisible = $odb->prepare('UPDATE EMBALLE_MESSAGES SET visible=0 AND conversation="potesasha"');
    $updatevisible->execute();

    header('Location: gestion_messages.php');
  }

  // On rend invisible tous les messages
  if(isset($_GET['razcamillesasha']))
  {
    $updatevisible = $odb->prepare('UPDATE EMBALLE_MESSAGES SET visible=0 AND conversation="camillesasha"');
    $updatevisible->execute();

    header('Location: gestion_messages.php');
  }

  // On rend invisible tous les messages
  if(isset($_GET['razhugosasha']))
  {
    $updatevisible = $odb->prepare('UPDATE EMBALLE_MESSAGES SET visible=0 AND conversation="hugosasha"');
    $updatevisible->execute();

    header('Location: gestion_messages.php');
  }

  // On rend invisible tous les messages
  if(isset($_GET['raz']))
  {
    $updatevisible = $odb->prepare('UPDATE EMBALLE_MESSAGES SET visible=0');
    $updatevisible->execute();

    header('Location: gestion_messages.php');
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Emballe Moi | Gestion des Messages</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">
  <!-- Jquery Lib -->
  <script src="../js/jquery-3.3.1.min.js"></script>
  <!-- Socket.io Lib -->
  <script src="../socket.io/socket.io.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="profile.php"><b>Emballe </b>Moi</a>
  </div>
              
  <div class="register-box-body">
    <p class="login-box-msg">Gestion des messages : </p>
    <div class="col-lg">

      <a href="gestion_questions.php" class="btn btn-info btn-block btn-flat">Retour</a>

      <br />

      <a href="transition.php" class="page">
        <button class="btn btn-info btn-block btn-flat">Envoyer transition</button>
      </a>

      <br />

      <a href="gestion_messages.php?suivantpotesacha">
        <button class="btn btn-success btn-block btn-flat">Passer au message suivant pote/Sasha</button>
      </a>

      <br />

      <a href="gestion_messages.php?suivantcamillesacha">
        <button class="btn btn-success btn-block btn-flat">Passer au message suivant Camille/Sasha</button>
      </a>

      <br />

      <a href="gestion_messages.php?suivanthugosacha">
        <button class="btn btn-success btn-block btn-flat">Passer au message suivant Hugo/Sasha</button>
      </a>

      <br />

      <a href="gestion_messages.php?razpotesasha">
        <button class="btn btn-warning btn-block btn-flat" name="raz">Remise à zéro des messages pote/Sasha</button>
      </a>

      <br />

      <a href="gestion_messages.php?razcamillesasha">
        <button class="btn btn-warning btn-block btn-flat" name="raz">Remise à zéro des messages Camille/Sasha</button>
      </a>

      <br />

      <a href="gestion_messages.php?razhugosasha">
        <button class="btn btn-warning btn-block btn-flat" name="raz">Remise à zéro des messages Hugo/Sasha</button>
      </a>

      <br />

      <a href="gestion_messages.php?raz">
        <button class="btn btn-danger btn-block btn-flat" name="raz">Remise à zéro de tous les messages</button>
      </a>
    </div>
  </div>
  <!-- /.register-box -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<script>
    (function($) {

        "use strict";

        $(function() {
            /////////////////////////////////////
            //Pour la partie USER (côté client)
            var socket = io.connect('http://192.168.1.30:3000');

            // Au clic sur le href
            $('.page').on('click', function(event) {

                event.preventDefault();

                var url = $(this).attr('href');

                // On créer le flag "emitVibration" avec la valeur 1
                socket.emit('&emitUrl', {url: url});
                console.log("emitUrl");

                return false;
            });
        });
    })(jQuery);
</script>
</body>
</html>
