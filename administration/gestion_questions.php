<?php 
  session_start();

  require_once '../includes/config.php';

  if (!(isset($_SESSION['admin']))) {
    header('Location: connexion.php');
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Emballe Moi | Gestion des Questions</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">
  <!-- Jquery Lib -->
  <script src="../js/jquery-3.3.1.min.js"></script>
  <!-- Socket.io Lib -->
  <script src="../socket.io/socket.io.js"></script>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
    p {
      color: #fff;
    }
    table {
      color: #fff;
    }
    .register-box-body {
      overflow: scroll;
    }
    .disabled {
      pointer-events: none;
      cursor: default;
    }
  </style>
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="profile.php"><b>Emballe </b>Moi</a>
  </div>
  <a href="gestion.php" class="btn btn-info btn-block btn-flat">Retour</a>
<?php
  if (isset($_GET['supprimer'])) {

    $idquestion = htmlspecialchars($_GET['supprimer']);

    $count = $odb->prepare('SELECT COUNT(*) FROM questions WHERE id=:id');
    $count->execute(array('id' => $idquestion));
    $infosCount = $count->fetchColumn(0);

    if($infosCount > 0)
    {
      $req = $odb -> prepare('DELETE FROM questions WHERE id = :id');
      $req -> execute(array(':id' => $idquestion));

      $req = $odb -> prepare('DELETE FROM reponses WHERE idquestion = :id');
      $req -> execute(array(':id' => $idquestion));

      echo '<div class="alert alert-success fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Success!</strong> La question et les réponses associées ont bien été supprimés !</div>';
    }
    else
    {
      echo '<div class="alert alert-danger fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Erreur!</strong> La question n\'existe pas</div>';
    }
  }

  if (isset($_GET['supprimerr'])) {

    $idreponse = htmlspecialchars($_GET['supprimerr']);

    $count = $odb->prepare('SELECT COUNT(*) FROM reponses WHERE id=:id');
    $count->execute(array('id' => $idreponse));
    $infosCount = $count->fetchColumn(0);

    if($infosCount > 0)
    {
      $req = $odb -> prepare('DELETE FROM reponses WHERE id = :id');
      $req -> execute(array(':id' => $idreponse));

      echo '<div class="alert alert-success fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Success!</strong> La réponse à bien été supprimée!</div>';
    }
    else
    {
      echo '<div class="alert alert-danger fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Erreur!</strong> La réponse n\'existe pas !</div>';
    }
  }

  if (isset($_GET['activer'])) {

      // On récupère l'id de la question
      $idactiver = htmlspecialchars($_GET['activer']);

      // On passe visible = 0 pour l'ancienne question

      $req = $odb -> prepare('UPDATE questions SET visible = 0 WHERE visible = 1');
      $req -> execute();

      $tempsActuel = time();

      $req = $odb -> prepare('UPDATE questions SET visible = 1, temps=:temps WHERE id = :idquestion');
      $req -> execute(array('temps' => $tempsActuel, ':idquestion' => $idactiver));
    }
?>
  <br>
  <div class="register-box-body">
    <a href="questions.php" class="btn btn-info btn-block btn-flat refresh" name="Refresh">Recharger</a>
    <p class="login-box-msg">Gestion des questions : </p>
    <div class="col-lg">
      <a href="ajouter_questions.php" class="btn btn-warning btn-block btn-flat">Ajouter question</a>
      <table class="table">
    <thead>
      <tr>
        <th>Position</th>
        <th>Question</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php
        $listequestions = $odb -> prepare('SELECT * FROM questions ORDER BY position ASC');
        $listequestions -> execute(array());

        while($infosquestions = $listequestions -> fetch())
        {
          $idquestion = $infosquestions['id'];
          $position   = $infosquestions['position'];
          $question   = $infosquestions['question'];
          $visible    = $infosquestions['visible'];

          if ($visible == 1) {
            $visible  = '<img src="../dist/img/power_on.png"';
            $disabled  = 'class="disabled"';
          }
          else
          {
            $visible  = '<img src="../dist/img/power_off.png"';
            $disabled = '';
          }
    ?>
      <tr>
        <td><?php echo $position; ?></td>
        <td><?php echo $question; ?></td>
        <td>
          <a href="edit_question.php?id=<?php echo $idquestion; ?>"><img src="../dist/img/edit.png"></a>
          <a href="gestion_questions.php?supprimer=<?php echo $idquestion; ?>" onclick="return confirm('Êtes-vous sur ?')"><img src="../dist/img/delete.png"></a>
          <a <?php echo $disabled; ?> href="gestion_questions.php?activer=<?php echo $idquestion; ?>"><?php echo $visible; ?></a>
        </td>
      </tr>
    <?php
    }
    ?>
    </tbody>
  </table>
    </div>
  </div>
  <br>
  <div class="register-box-body">
    <p class="login-box-msg">Gestion des réponses : </p>
    <div class="col-lg">
      <a href="ajouter_reponses.php" class="btn btn-warning btn-block btn-flat">Ajouter reponse</a>
      <table class="table">
    <thead>
      <tr>
        <th>Question</th>
        <th>Reponse</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php
        $listereponses = $odb -> prepare('SELECT reponses.id AS id, question, reponse FROM reponses INNER JOIN questions ON reponses.idquestion = questions.id ORDER BY questions.id');
        $listereponses -> execute(array());

        while($infosreponses = $listereponses -> fetch())
        {
          $idreponse = $infosreponses['id'];
          $nomquestion = $infosreponses['question'];
          $reponse = $infosreponses['reponse'];
    ?>
      <tr>
        <td><?php echo $nomquestion; ?></td>
        <td><?php echo $reponse; ?></td>
        <td>
          <a href="edit_reponse.php?id=<?php echo $idreponse; ?>"><img src="../dist/img/edit.png"></a>
          <a href="gestion_questions.php?supprimerr=<?php echo $idreponse; ?>"><img src="../dist/img/delete.png"></a>
        </td>
      </tr>
    <?php
    }
    ?>
    </tbody>
  </table>
    </div>
  </div>
</div>
  <!-- /.register-box -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript">
  var elems = document.getElementsByClassName('confirmation');
  var confirmIt = function (e) {
      if (!confirm('Are you sure?')) e.preventDefault();
  };
  for (var i = 0, l = elems.length; i < l; i++) {
      elems[i].addEventListener('click', confirmIt, false);
  }
</script>
<script>
    (function($) {

        "use strict";

        $(function() {
            /////////////////////////////////////
            //Pour la partie USER (côté client)
            var socket = io.connect('http://172.16.100.166:3000');

            // Au clic sur le href
            $('.refresh').on('click', function(event) {

                event.preventDefault();

                var url = $(this).attr('href');

                // On créer le flag "emitVibration" avec la valeur 1
                socket.emit('&emitUrl', {url: url});
                console.log("emitUrl");

                return false;
            });
        });
    })(jQuery);
</script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
