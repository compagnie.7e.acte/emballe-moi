<?php
	require_once("../includes/config.php");
	require_once("../jpgraph-4.2.0/src/jpgraph.php");
	require_once("../jpgraph-4.2.0/src/jpgraph_pie.php");

	// Tableaux de données destinées à JpGraph
	$tableauReponses = array();
	$tableauNombre = array();
	$pourcentage = array();

	$selectQuestions = $odb -> prepare('SELECT * FROM questions WHERE visible = 1');
	$selectQuestions -> execute();
	
	while($questions = $selectQuestions->fetch())
	{
		$selectReponses = $odb -> prepare('SELECT * FROM reponses WHERE idquestion = :idquestion');
		$selectReponses -> execute(array('idquestion' => $questions['id']));

		$cpt = 0;

		while ($reponses = $selectReponses -> fetch())
		{
			// On ajoute les libellés des stats
			$tableauReponses[$cpt] = $reponses['reponse'];

			// On compte le nombre pour une réponse
			$countresultat = $odb -> prepare('SELECT COUNT(*) FROM resultats WHERE idreponse=:idreponse');
			$countresultat -> execute(array(':idreponse' => $reponses['id'] ));
			$nbResultat = $countresultat->fetchColumn(0);

			// On ajoute les valeurs des stats
			$tableauNombre[$cpt] = $nbResultat;

			$countTotal = $odb -> prepare('SELECT COUNT(*) FROM resultats WHERE idquestion=:idquestion');
			$countTotal -> execute(array(':idquestion' => $questions['id'] ));
			$nbTotal = $countTotal->fetchColumn(0);

			$pourcentage[$cpt] = round($nbResultat/$nbTotal*100);

			$cpt = $cpt+1;
		}

		// On spécifie la largeur et la hauteur du graphique conteneur&#160;
		$graph = new PieGraph(400,300);

		// Titre du graphique
		$graph->title->Set($questions['question']);

		// Créer un graphique secteur (classe PiePlot)
		$oPie = new PiePlot($pourcentage);

		// Légendes qui accompagnent chaque secteur, ici chaque année
		$oPie->SetLegends($tableauReponses);

		// position du graphique (légèrement à droite)
		$oPie->SetCenter(0.4); 

		$oPie->SetValueType(PIE_VALUE_ABS);

		// Format des valeurs de type entier
		$oPie->value->SetFormat('%d&#37;');

		// Ajouter au graphique le graphique secteur
		$graph->Add($oPie);

		// Provoquer l'affichage (renvoie directement l'image au navigateur)
		$graph->Stroke();
	}
?>