<?php 
  session_start();

  require_once '../includes/config.php';

  if (!(isset($_SESSION['admin']))) {
    header('Location: connexion.php');
  }

  if (!(isset($_SESSION['admin']))) {
    header('Location: connexion.php');
  }

  if(isset($_GET['editer']))
  {
    $idmessage = htmlspecialchars($_GET['editer']);

    if(isset($_POST['btn-edit']))
    {
      $idnewmessage = htmlspecialchars($_POST['idmessage']);
      $message = $_POST['message'];
      $type = htmlspecialchars($_POST['type']);
      $conversation = htmlspecialchars($_POST['conversation']);

      $countMessage = $odb->prepare('SELECT COUNT(*) FROM EMBALLE_MESSAGES WHERE idmessage=:idmessage');
      $countMessage->execute(array('idmessage' => $idmessage));
      $nbMessage = $countMessage->fetchColumn(0);

      if($nbMessage == 1)
      {
        if($idnewmessage == $idmessage)
        {
          // On met à jour
          $updatemessage = $odb->prepare('UPDATE EMBALLE_MESSAGES SET message=:message, type=:type, conversation=:conversation WHERE idmessage=:idmessage');
          $updatemessage->execute(array('message' => $message, 'type' => $type, 'conversation' => $conversation, 'idmessage' => $idmessage));
        }

        else
        {
          $countNewMessage = $odb->prepare('SELECT COUNT(*) FROM EMBALLE_MESSAGES WHERE idmessage=:idmessage');
          $countNewMessage->execute(array('idmessage' => $idnewmessage));
          $nbNewMessage = $countNewMessage->fetchColumn(0);

          if($nbNewMessage != 1)
          {
            // On met à jour
            $updatemessage = $odb->prepare('UPDATE EMBALLE_MESSAGES SET idmessage=:idnewmessage, message=:message, type=:type, conversation=:conversation WHERE idmessage=:idmessage');
            $updatemessage->execute(array('idnewmessage' => $idnewmessage, 'message' => $message, 'type' => $type, 'conversation' => $conversation, 'idmessage' => $idmessage));

            header('Location: modif_messages.php');
          }

          else
          {
            $erreur = "Ce numéro de message existe déjà";
          }
        }
      }
    }
  }

  if(isset($_GET['supprimer']))
  {
    $idmessage = htmlspecialchars($_GET['supprimer']);

    $deletemessage = $odb->prepare('DELETE FROM EMBALLE_MESSAGES WHERE idmessage=:idmessage');
    $deletemessage->execute(array('idmessage' => $idmessage));

    header('Location: modif_messages.php');
  }

  if(isset($_POST['btn-ajouter']))
  {
    $idmessage = htmlspecialchars($_POST['idmessage']);

    $countMessage = $odb->prepare('SELECT COUNT(*) FROM EMBALLE_MESSAGES WHERE idmessage=:idmessage');
    $countMessage->execute(array('idmessage' => $idmessage));
    $nbMessage = $countMessage->fetchColumn(0);

    if($nbMessage == 1)
    {
      $erreur = 'Cet identifiant de message existe déjà';
    }

    else
    {
      $message = $_POST['message'];
      $type = htmlspecialchars($_POST['type']);
      $conversation = htmlspecialchars($_POST['conversation']);

      $insertmessage = $odb->prepare('INSERT INTO EMBALLE_MESSAGES (idmessage, message, visible, type, conversation) VALUES (:idmessage, :message, 0, :type, :conversation)');
      $insertmessage->execute(array('idmessage' => $idmessage, 'message' => $message, 'type' => $type, 'conversation' => $conversation));

      header('Location: modif_messages.php');
    }
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Emballe Moi | Gestion des messages</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

  <!-- Emoji -->
  <link href="../pages/emoji.css" rel="stylesheet">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
    p {
      color: #fff;
    }
    table {
      color: #fff;
    }
    .register-box-body {
      overflow: scroll;
    }
    .disabled {
      pointer-events: none;
      cursor: default;
    }
  </style>
</head>
<body class="hold-transition register-page">
<div class="register-box" style="width:100%">
  <div class="register-logo">
    <a href="profile.php"><b>Emballe </b>Moi</a>
  </div>
  
  <div class="register-box-body">

    <a href="gestion.php" class="btn btn-default btn-block btn-flat">Retour</a><br>
    <?php
    if(isset($_GET['editer']))
    {
      if(isset($erreur))
        echo $erreur;

      $idmessage = htmlspecialchars($_GET['editer']);

      $countMessage = $odb->prepare('SELECT COUNT(*) FROM EMBALLE_MESSAGES WHERE idmessage=:idmessage');
      $countMessage->execute(array('idmessage' => $idmessage));
      $nbMessage = $countMessage->fetchColumn(0);

      if($nbMessage == 1)
      {
        $selectmessage = $odb->prepare('SELECT * FROM EMBALLE_MESSAGES WHERE idmessage=:idmessage');
        $selectmessage->execute(array('idmessage' => $idmessage));
        $infosmessage = $selectmessage->fetch();
    ?>
        <p class="login-box-msg">Editer un message : </p>
        <div class="col-lg">
          <form method="POST">
            <div class="form-group has-feedback">
              <label>Position</label>
              <input type="text" class="form-control" placeholder="Position du message" name="idmessage" id="idmessage" value="<?php echo $infosmessage['idmessage']; ?>" autofocus>
              <span class="fa fa-sort-amount-desc form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
              <label>Message</label>
              <textarea class="form-control" rows="5" name="message" id="message" ><?php echo $infosmessage['message']; ?></textarea>
              <span class="fa fa-comment form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
              <label>Type</label>
              <select class="form-control" placeholder="Type" name="type" id="type">
                <?php
                  if($infosmessage['type'] == "affichage")
                  {
                    echo '<option value="affichage" selected>Affichage</option>';
                    echo '<option value="ecriture">Écriture</option>';
                  }

                  else
                  {
                    echo '<option value="affichage">Affichage</option>';
                    echo '<option value="ecriture" selected>Écriture</option>';
                  }
                ?>      
              </select>
            </div>

            
            <div class="form-group has-feedback">
              <label>Type</label>
              <select class="form-control" placeholder="Type" name="conversation" id="conversation">
                <?php
                  if($infosmessage['conversation'] == "potesacha")
                  {
                ?>
                    <option value="potesacha" selected>Pote/Sacha</option>
                    <option value="camillesacha">Camille/Sacha</option>
                    <option value="hugosacha">Hugo/Sacha</option>
                <?php
                  }

                  else if($infosmessage['conversation'] == "camillesacha")
                  {
                ?>
                    <option value="potesacha">Pote/Sacha</option>
                    <option value="camillesacha" selected>Camille/Sacha</option>
                    <option value="hugosacha">Hugo/Sacha</option>
                <?php
                  }

                  else if($infosmessage['conversation'] == "hugosacha")
                  {
                ?>
                    <option value="potesacha">Pote/Sacha</option>
                    <option value="camillesacha">Camille/Sacha</option>
                    <option value="hugosacha" selected>Hugo/Sacha</option>
                <?php
                  }
                ?>
              </select>
            </div>

            <div class="form-group has-feedback">
              <button type="submit" class="btn btn-primary btn-block btn-flat" name="btn-edit" id="btn-edit">Modifier</button>
            </div>
          </form>
        </div>
    <?php
      }

      else
      {
        echo 'Ce message n\'existe pas !';
      }
    }

    else if(isset($_GET['ajouter']))
    {
      if(isset($erreur))
        echo $erreur;
    ?>
      <p class="login-box-msg">Ajouter un message : </p>
      <div class="col-lg">
        <form method="POST">
          <div class="form-group has-feedback">
            <label>Position</label>
            <input type="text" class="form-control" placeholder="Position du message" name="idmessage" id="idmessage" autofocus>
            <span class="fa fa-sort-amount-desc form-control-feedback"></span>
          </div>

          <div class="form-group has-feedback">
            <label>Personnages</label>
            <select class="form-control" placeholder="Type" name="type">
              <option value="2">Sacha</option>
              <option value="1">Autres</option>
            </select>
            <span class="fa fa-question-circle form-control-feedback"></span>
          </div>

          <div class="form-group has-feedback">
            <label>Message</label>
            <input type="text" class="form-control" placeholder="Message" name="message" id="message" autofocus>
            <span class="fa fa-comment form-control-feedback"></span>
          </div>

          <div class="form-group has-feedback">
            <label>Type</label>
            <select class="form-control" placeholder="Type" name="type">
              <option value="affichage">Affichage</option>
              <option value="ecriture">Écriture</option>
            </select>
            <span class="fa fa-question-circle form-control-feedback"></span>
          </div>

          <div class="form-group has-feedback">
            <label>Conversation</label>
            <select class="form-control" placeholder="Conversation" name="conversation">
              <option value="potesacha">Pote/Sacha</option>
              <option value="camillesacha">Camille/Sacha</option>
              <option value="hugosacha">Hugo/Sacha</option>
            </select>
            <span class="fa fa-question-circle form-control-feedback"></span>
          </div>

          <div class="form-group has-feedback">
            <button type="submit" class="btn btn-primary btn-block btn-flat" name="btn-ajouter">Ajouter</button>
          </div>
        </form>
      </div>
    <?php
    }

    else
    {
    ?>
      <p class="login-box-msg">Gestion des messages : </p>
      <div class="col-lg">
        <a href="modif_messages.php?ajouter" class="btn btn-warning btn-block btn-flat">Ajouter messages</a>
        <table class="table">
          <thead>
            <tr>
              <th>N° message</th>
              <th>Message</th>
              <th>Type</th>
              <th>Conversation</th>
            </tr>
          </thead>
          <tbody>
          <?php
              $selectmessage = $odb -> prepare('SELECT * FROM EMBALLE_MESSAGES ORDER BY idmessage');
              $selectmessage -> execute(array());

              while($infosmessage = $selectmessage -> fetch())
              {
            ?>
                <tr>
                  <td><?php echo $infosmessage['idmessage']; ?></td>
                  <td><?php echo $infosmessage['message']; ?></td>
                  <td>
                    <?php
                      if($infosmessage['type'] == "ecriture")
                        echo 'Écriture';

                      else
                        echo 'Affichage';
                    ?>
                  </td>
                  <td>
                    <?php
                      if($infosmessage['conversation'] == "potesacha")
                        echo 'Pote/Sacha';

                      else if($infosmessage['conversation'] == "camillesacha")
                        echo 'Camille/Sacha';

                      else if($infosmessage['conversation'] == "hugosacha")
                        echo 'Hugo/Sacha';
                    ?>
                  </td>
                  <td>
                    <a href="modif_messages.php?editer=<?php echo $infosmessage['idmessage']; ?>"><img src="../dist/img/edit.png"></a>
                    <a href="modif_messages.php?supprimer=<?php echo $infosmessage['idmessage']; ?>"><img src="../dist/img/delete.png"></a>
                  </td>
                </tr>
            <?php
              }
            ?>
          </tbody>
        </table>
      </div>
    <?php
    }
    ?>
  </div>
</div>
  <!-- /.register-box -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript">
  var elems = document.getElementsByClassName('confirmation');
  var confirmIt = function (e) {
      if (!confirm('Are you sure?')) e.preventDefault();
  };
  for (var i = 0, l = elems.length; i < l; i++) {
      elems[i].addEventListener('click', confirmIt, false);
  }
</script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>

