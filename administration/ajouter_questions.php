<?php 
  session_start();

  require_once '../includes/config.php';

  if (!(isset($_SESSION['admin']))) {
    header('Location: connexion.php');
  }

  if(isset($_POST['btn-question']))
  {
    $position = htmlspecialchars($_POST['position']);
    $question = htmlspecialchars($_POST['question']);
    $type = htmlspecialchars($_POST['type']);

    $insertquestion = $odb->prepare('INSERT INTO questions (position, question, type) VALUES (:position, :question, :type)');
    $insertquestion->execute(array('position' => $position, 'question' => $question, 'type' => $type));

    $resultat = '<div class="alert alert-success fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Success!</strong> La question a bien été ajouté !</div>';
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Emballe Moi | Ajouter une question</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

  <style>
    p
    {
      color:#fff;
    }
  </style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="profile.php"><b>Emballe </b>Moi</a>
  </div>

  <div class="register-box-body">
    <?php
      if(isset($resultat))
        echo $resultat;
    ?>
    <p class="login-box-msg">Ajouter une question</p>

    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Position" name="position" autofocus>
        <span class="fa fa-sort-amount-desc form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Question" name="question">
        <span class="fa fa-question-circle form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <label>Type</label>
        <select class="form-control" placeholder="Type" name="type">
          <option value="simple">Choix simple</option>
          <option value="multiple">Choix multiple</option>
        </select>
        <span class="fa fa-question-circle form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
          <button type="submit" class="btn btn-primary btn-block btn-flat" name="btn-question">Ajouter</button>
      </div>
      <a href="gestion_questions.php" class="btn btn-info btn-block btn-flat">Retour</a>
    </form>
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
