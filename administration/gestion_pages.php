<?php 
  session_start();

  require_once '../includes/config.php';

  if (!(isset($_SESSION['admin']))) {
    header('Location: connexion.php');
  }

  require_once 'generation_stats.php';
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Emballe Moi | Gestion des Questions</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
    p {
      color: #fff;
    }
    table {
      color: #fff;
    }
    .register-box-body {
      overflow: scroll;
    }
    .disabled {
      pointer-events: none;
      cursor: default;
    }
  </style>
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="profile.php"><b>Emballe </b>Moi</a>
  </div>
  <a href="gestion.php" class="btn btn-info btn-block btn-flat">Retour</a>
  <br>
  <div class="register-box-body">
    <p class="login-box-msg">Gestion des pages : </p>
    <div class="col-lg">
      <table class="table">
    <thead>
      <tr>
        <th>Position</th>
        <th>Page</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    <?php
          $visible  = '<img src="../dist/img/power_on.png">';
          $invisible  = '<img src="../dist/img/power_off.png">';
    ?>
      <tr>
        <td>1</td>
        <td>Profil</td>
        <td>
          <?php
            echo '<a class="page" href="profil.php">' . $visible . '</a>';
          ?> 
        </td>
      </tr>
      <tr>
        <td>2</td>
        <td>Avatar</td>
        <td>
          <?php
            echo '<a class="page" href="avatar.php">' . $visible . '</a>';
          ?> 
        </td>
      </tr>
      <tr>
        <td>3</td>
        <td>Snake</td>
        <td>
          <?php
            echo '<a class="page" href="snake.php">' . $visible . '</a>';
          ?> 
        </td>
      </tr>
      <tr>
        <td>4</td>
        <td>Chat</td>
        <td>
          <?php
            echo '<a class="page" href="chat.php">' . $visible . '</a>';
          ?> 
        </td>
      </tr>
      <tr>
        <td>5</td>
        <td>Webcam</td>
        <td>
          <?php
            echo '<a class="page" href="webcam.php">' . $visible . '</a>';
          ?> 
        </td>
      </tr>
      <tr>
        <td>6</td>
        <td>Vibrations</td>
        <td>
          <?php
            echo '<a class="page" href="vibrations.php">' . $visible . '</a>';
          ?> 
        </td>
      </tr>
      <tr>
        <td>7</td>
        <td>Questions Live</td>
        <td>
          <?php
            echo '<a class="page" href="questions.php">' . $visible . '</a>';
          ?> 
        </td>
      </tr>
      <tr>
        <td>8</td>
        <td>PDF</td>
        <td>
          <?php
            echo '<a class="page" href="pdf.php">' . $visible . '</a>';
          ?> 
        </td>
      </tr>
    </tbody>
  </table>
    </div>
  </div>
</div>
  <!-- /.register-box -->

<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Jquery Lib -->
  <script src="../js/jquery-3.3.1.min.js"></script>
  <!-- Socket.io Lib -->
  <script src="../socket.io/socket.io.js"></script>
<script>
    (function($) {

        "use strict";

        $(function() {
            /////////////////////////////////////
            //Pour la partie USER (côté client)
            var socket = io.connect('http://172.16.1.3:3000');

            // Au clic sur le href
            $('.page').on('click', function(event) {

                event.preventDefault();

                var url = $(this).attr('href');

                // On créer le flag "emitVibration" avec la valeur 1
                socket.emit('&emitUrl', {url: url});
                console.log("emitUrl");

                return false;
            });
        });
    })(jQuery);
</script>
<script>
    (function($) {

        "use strict";

        $(function() {
            /////////////////////////////////////
            //Pour la partie USER (côté client)
            var socket = io.connect('http://172.16.1.4:3000');

            // Au clic sur le href
            $('.page').on('click', function(event) {

                event.preventDefault();

                var url = $(this).attr('href');

                // On créer le flag "emitVibration" avec la valeur 1
                socket.emit('&emitUrl', {url: url});
                console.log("emitUrl");

                return false;
            });
        });
    })(jQuery);
</script>
</body>
</html>
  