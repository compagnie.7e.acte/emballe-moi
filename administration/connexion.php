<?php 
  
  require_once '../includes/config.php';

  $query = $odb->prepare("SELECT `actif` FROM `config`");
  $query->execute();
  $actif = $query -> fetchColumn(0);

  if (isset($_SESSION['admin'])) {
    header('Location: gestion.php');
  }

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Emballe Moi | Connexion</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition lockscreen">
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
    <a href="../index2.html"><b>Emballe</b>Moi</a>
  </div>

          <?php
          if (isset($_POST['connexionBtn'])) {
            
            // On récupère le password en POST
            $password = $_POST['password'];

            // On récupère le password depuis la BDD
            $query = $odb->prepare("SELECT `password` FROM `config`");
            $query->execute();
            $actif = $query -> fetch();

            // Si les mots de passe correspondent, on créer une session admin
            if ($actif['password'] == $password)
            {
              $_SESSION['admin'] = 'admin';

              echo '<div class="alert alert-success fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Success!</strong> Connexion acceptée, redirection...<meta http-equiv="refresh" content="3;url=gestion.php"> !</div>';
            }
            else
            {
              echo '<div class="alert alert-danger fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Erreur!</strong> Mot de passe incorrect !</div>'; 
            }

          }
        ?>
  <!-- User name -->
  <div class="lockscreen-name">Connexion</div>

  <!-- START LOCK SCREEN ITEM -->
  <div class="lockscreen-item">
    <!-- lockscreen image -->
    <div class="lockscreen-image">
      <img src="../dist/img/7acte.jpg" alt="User Image">
    </div>
    <!-- /.lockscreen-image -->

    <!-- lockscreen credentials (contains the form) -->
    <form action="" method="post" class="lockscreen-credentials">
      <div class="input-group">
        <input type="password" class="form-control" name="password" placeholder="Mot de passe">

        <div class="input-group-btn">
          <button type="submit" name="connexionBtn" class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
        </div>
      </div>
    </form>
    <!-- /.lockscreen credentials -->

  </div>
  <!-- /.lockscreen-item -->
</div>
<!-- /.center -->

<!-- jQuery 3 -->
<script src="/../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
