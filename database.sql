-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 02 Mars 2018 à 15:39
-- Version du serveur :  5.7.21-0ubuntu0.16.04.1
-- Version de PHP :  7.0.25-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `emballe_moi`
--

-- --------------------------------------------------------

--
-- Structure de la table `config`
--

CREATE TABLE `config` (
  `actif` int(1) NOT NULL,
  `password` varchar(255) CHARACTER SET ucs2 NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `config`
--

INSERT INTO `config` (`actif`, `password`, `status`) VALUES
(1, 'compagnie', 8);

-- --------------------------------------------------------

--
-- Structure de la table `EMBALLE_MESSAGES`
--

CREATE TABLE `EMBALLE_MESSAGES` (
  `idmessage` int(11) NOT NULL,
  `idmembre` int(11) NOT NULL,
  `message` text NOT NULL,
  `visible` int(11) NOT NULL,
  `type` varchar(30) NOT NULL,
  `conversation` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `EMBALLE_MESSAGES`
--

INSERT INTO `EMBALLE_MESSAGES` (`idmessage`, `idmembre`, `message`, `visible`, `type`, `conversation`) VALUES
(10, 1, 'Salut les potoss ! Perso, j\'ai passé l\'été le plus ouf de toute ma life ! j\'ai quiffé à mort !&lt;br /&gt;Si j\'vous raconte, vous aller être dég !', 1, 'affichage', 'potesacha'),
(20, 2, 'ben vas\'y, raconte ! t\'as chopé ?', 2, 'ecriture', 'potesacha'),
(40, 2, 'ben vas\'y, raconte ! t\'as chopé ?', 1, 'affichage', 'potesacha'),
(50, 1, 'mieux qu\'ça !', 0, 'affichage', 'potesacha'),
(60, 2, 'allez, crache ta valda!', 0, 'ecriture', 'potesacha'),
(70, 2, 'allez, crache ta valda!', 0, 'affichage', 'potesacha'),
(80, 1, 'Si tu veux vraiment savoir, télécharge cette application... j\'ai préparé une p\'tite surprise !', 0, 'affichage', 'potesacha'),
(90, 2, 'mais qu\'est-c\'tu fous ?', 0, 'ecriture', 'potesacha'),
(100, 2, 'mais qu\'est-c\'tu fous ?', 0, 'affichage', 'potesacha'),
(110, 1, 'j\'te jure ! vas-y ! faut absolument qu\'tu l\'fasses ! c\'est un truc de dingue !', 0, 'affichage', 'potesacha'),
(120, 2, 'tu m\'saoules...', 0, 'ecriture', 'potesacha'),
(130, 2, 'tu m\'saoules...', 0, 'affichage', 'potesacha'),
(140, 1, 'allez ! t\'en as pour 2 mn... on va bien s\'marrer !', 0, 'affichage', 'potesacha'),
(150, 2, 'mais c\'est quoi ton truc ???', 0, 'ecriture', 'potesacha'),
(160, 2, 'mais c\'est quoi ton truc ???', 0, 'affichage', 'potesacha'),
(170, 1, 'mais t\'es relou ! j\'te dis qu\'si tu veux savoir, tu télécharges !... Sinon tu dégages !', 0, 'affichage', 'potesacha'),
(180, 2, 'ça va... t\'énerves pas...', 0, 'ecriture', 'potesacha'),
(190, 2, 'ça va... t\'énerves pas...', 0, 'affichage', 'potesacha'),
(200, 1, 'bon alors ça y est ?', 0, 'affichage', 'potesacha'),
(210, 2, 'ouais, ouais, t\'inquètes... je charge...', 0, 'ecriture', 'potesacha'),
(220, 2, 'ouais, ouais, t\'inquètes... je charge...', 0, 'affichage', 'potesacha'),
(230, 1, 'et toi, sinon, CT cool tes vacances ?', 0, 'affichage', 'potesacha'),
(240, 2, 'bof... normal... rien d\'spécial...', 0, 'ecriture', 'potesacha'),
(250, 2, 'bof... normal... rien d\'spécial...', 0, 'affichage', 'potesacha'),
(260, 1, 'Moi j\'suis sorti avec la meuf la plus canon de la colo...', 0, 'affichage', 'potesacha'),
(270, 2, 'serieux ?', 0, 'ecriture', 'potesacha'),
(280, 2, 'serieux ?', 0, 'affichage', 'potesacha'),
(290, 1, 'j\'te jure !', 0, 'affichage', 'potesacha'),
(300, 2, 'comment elle s\'apelle ?', 0, 'ecriture', 'potesacha'),
(310, 2, 'comment elle s\'apelle ?', 0, 'affichage', 'potesacha'),
(320, 1, 'Laura', 0, 'affichage', 'potesacha'),
(330, 2, 'joli...', 0, 'ecriture', 'potesacha'),
(340, 2, 'joli...', 0, 'affichage', 'potesacha'),
(350, 1, 'ouais... t\'as téléchargé ? ça y est ?', 0, 'affichage', 'potesacha'),
(360, 2, 'ouais, bientôt fini...', 0, 'ecriture', 'potesacha'),
(370, 2, 'ouais, bientôt fini...', 0, 'affichage', 'potesacha'),
(380, 1, 'ok. J\'t\'envoie des sms qu\'elle m\'a envoyé... Tu vas voir comment elle me kiffe !', 0, 'affichage', 'potesacha'),
(390, 2, 't\'es qu\'un gros mytho !', 0, 'ecriture', 'potesacha'),
(400, 2, 't\'es qu\'un gros mytho !', 0, 'affichage', 'potesacha'),
(410, 1, 'j\'ai des preuves !!', 0, 'affichage', 'potesacha'),
(420, 2, 'vas\'y envoie !', 0, 'ecriture', 'potesacha'),
(430, 2, 'vas\'y envoie !', 0, 'affichage', 'potesacha'),
(440, 1, 'c\'est parti !!', 0, 'affichage', 'potesacha'),
(450, 1, 'CT trop bien la soirée !', 1, 'affichage', 'camillesacha'),
(460, 2, 'Ouais, grave !', 2, 'ecriture', 'camillesacha'),
(470, 2, 'Ouais, grave !', 1, 'affichage', 'camillesacha'),
(480, 1, 't’es vraiment trop cooool ! <img src="emojis/coeur.png"/><img src="emojis/coeur.png"/><img src="emojis/coeur.png"/>', 1, 'affichage', 'camillesacha'),
(490, 2, 'toi aussi ! <i class="em em-heart"></i>', 2, 'ecriture', 'camillesacha'),
(500, 2, 'toi aussi ! <i class="em em-heart"></i>', 1, 'affichage', 'camillesacha'),
(510, 1, 'keur sur toi ! <i class="em em-heart"></i>', 1, 'affichage', 'camillesacha'),
(520, 1, 'alors ? qu’est ce t’en dis ?', 0, 'affichage', 'potesacha'),
(530, 2, 'ouais… pas mal !... Envoi une photo !', 0, 'ecriture', 'potesacha'),
(540, 2, 'ouais… pas mal !... Envoi une photo !', 0, 'affichage', 'potesacha'),
(550, 1, 'ok', 0, 'affichage', 'potesacha'),
(560, 1, 'Photo', 0, 'affichage', 'potesacha'),
(570, 1, 'Salut tafiolle !', 0, 'affichage', 'hugosacha'),
(580, 2, 'TG', 0, 'ecriture', 'hugosacha'),
(590, 2, 'TG', 0, 'affichage', 'hugosacha'),
(600, 1, 'Tjs faché ?', 0, 'affichage', 'hugosacha'),
(610, 2, '<i class="em em-angry"></i>', 0, 'ecriture', 'hugosacha'),
(620, 2, '<i class="em em-angry"></i>', 0, 'affichage', 'hugosacha'),
(630, 1, 'j’y suis pour r1…', 0, 'affichage', 'hugosacha'),
(640, 2, 'quoi ???? …  tu rigoles ?', 0, 'ecriture', 'hugosacha'),
(650, 2, 'quoi ???? …  tu rigoles ?', 0, 'affichage', 'hugosacha'),
(660, 1, 'J’te rappelle que c’est toi qui a voulu !', 0, 'affichage', 'hugosacha'),
(670, 2, 'j’ai rien voulu du tout !!', 0, 'ecriture', 'hugosacha'),
(680, 2, 'j’ai rien voulu du tout !!', 0, 'affichage', 'hugosacha'),
(690, 1, 't’as pas voulu un rencart avec ma sœur peut-être ??', 0, 'affichage', 'hugosacha'),
(700, 2, 'si', 0, 'ecriture', 'hugosacha'),
(710, 2, 'si', 0, 'affichage', 'hugosacha'),
(720, 1, 'alors, tu vois !', 0, 'affichage', 'hugosacha'),
(730, 2, 'C’que j’vois, c’est qu’t’es une raclure…', 0, 'ecriture', 'hugosacha'),
(740, 2, 'C’que j’vois, c’est qu’t’es une raclure…', 0, 'affichage', 'hugosacha'),
(750, 1, 'arrête ou j’balance tout …', 0, 'affichage', 'hugosacha'),
(760, 2, 'si tu fais ça, t’auras ma mort sur la conscience.', 0, 'ecriture', 'hugosacha'),
(770, 2, 'si tu fais ça, t’auras ma mort sur la conscience.', 0, 'affichage', 'hugosacha'),
(780, 1, 'Que d’la gueule !', 0, 'affichage', 'hugosacha'),
(790, 2, 'c’est ça…', 0, 'ecriture', 'hugosacha'),
(800, 2, 'c’est ça…', 0, 'affichage', 'hugosacha'),
(810, 1, 'Sacha, t’es tjs là ?', 0, 'affichage', 'potesacha'),
(820, 2, 'ouaip', 0, 'ecriture', 'potesacha'),
(830, 2, 'ouaip', 0, 'affichage', 'potesacha'),
(840, 1, 'on s’fait un ciné avec Hugo dans 1h, tu viens ?', 0, 'affichage', 'potesacha'),
(850, 2, 'non j’peux pas…', 0, 'ecriture', 'potesacha'),
(860, 2, 'non j’peux pas…', 0, 'affichage', 'potesacha'),
(870, 1, 'pourquoi ???', 0, 'affichage', 'potesacha'),
(880, 2, 'Tkt…', 0, 'ecriture', 'potesacha'),
(890, 2, 'Tkt…', 0, 'affichage', 'potesacha'),
(900, 1, 'ok.', 0, 'affichage', 'potesacha'),
(910, 2, 'tu lui parles pas de Camille…', 0, 'ecriture', 'potesacha'),
(920, 2, 'tu lui parles pas de Camille…', 0, 'affichage', 'potesacha'),
(930, 1, '???', 0, 'affichage', 'potesacha'),
(940, 2, 'Il est jaloux… j’préfère pas …', 0, 'ecriture', 'potesacha'),
(950, 2, 'Il est jaloux… j’préfère pas …', 0, 'affichage', 'potesacha'),
(960, 1, 'ok. slt', 0, 'affichage', 'potesacha'),
(970, 2, '<i class="em em---1"></i>', 0, 'ecriture', 'potesacha'),
(980, 2, '<i class="em em---1"></i>', 0, 'affichage', 'potesacha'),
(990, 1, 'ça va ?', 0, 'affichage', 'hugosacha'),
(1000, 1, 't’es là ?', 0, 'affichage', 'hugosacha'),
(1010, 1, 'Sacha !... allez ! oublie un peu !...', 0, 'affichage', 'hugosacha'),
(1020, 1, 'on s’est bien marré qd même !', 0, 'affichage', 'hugosacha'),
(1030, 1, 'faut qu’j’te laisse… A +', 0, 'affichage', 'hugosacha');

-- --------------------------------------------------------

--
-- Structure de la table `questionnaire`
--

CREATE TABLE `questionnaire` (
  `id` int(11) NOT NULL,
  `sexe` int(11) DEFAULT NULL,
  `classe` int(11) DEFAULT NULL,
  `parent1` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `parent2` varchar(40) CHARACTER SET utf8 DEFAULT NULL,
  `telephone` int(11) DEFAULT NULL,
  `smartphone` int(11) DEFAULT NULL,
  `marque` varchar(40) DEFAULT NULL,
  `reseau` int(11) DEFAULT NULL,
  `listereseaux` varchar(100) DEFAULT NULL,
  `petitcopin` int(11) DEFAULT NULL,
  `combien` varchar(10) DEFAULT NULL,
  `commentrencontre` varchar(100) DEFAULT NULL,
  `rupture` varchar(100) DEFAULT NULL,
  `aborder` varchar(100) DEFAULT NULL,
  `determiner` varchar(100) DEFAULT NULL,
  `partager` varchar(100) DEFAULT NULL,
  `faireavec` varchar(100) DEFAULT NULL,
  `visitersite` int(11) DEFAULT NULL,
  `inscriresite` int(11) DEFAULT NULL,
  `recusextos` int(11) DEFAULT NULL,
  `envoiesextos` int(11) DEFAULT NULL,
  `victimeharcelement` int(11) DEFAULT NULL,
  `temoinharcelement` int(11) DEFAULT NULL,
  `reagi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `position` int(1) NOT NULL,
  `question` varchar(255) NOT NULL,
  `type` varchar(20) NOT NULL,
  `visible` int(1) NOT NULL DEFAULT '0',
  `temps` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `questions`
--

INSERT INTO `questions` (`id`, `position`, `question`, `type`, `visible`, `temps`) VALUES
(1, 1, 'Quel est ton sexe ??', 'simple', 0, 1519654896),
(10, 2, 'Quel âge as-tu ?', 'simple', 0, 1519993724),
(11, 3, 'L\'embrasse t-il ?', 'simple', 0, 1519993734),
(12, 4, 'Doit-il la violenter ?', 'simple', 1, 1519993735);

-- --------------------------------------------------------

--
-- Structure de la table `reponses`
--

CREATE TABLE `reponses` (
  `id` int(11) NOT NULL,
  `idquestion` int(11) NOT NULL,
  `reponse` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `reponses`
--

INSERT INTO `reponses` (`id`, `idquestion`, `reponse`) VALUES
(1, 1, 'Homme'),
(2, 1, 'Femme'),
(18, 10, '13 - 16 ans'),
(19, 10, '17 - 20 ans'),
(20, 10, '20 - 25 ans'),
(21, 10, '+ 25 ans'),
(22, 11, 'Oui'),
(23, 11, 'Non'),
(25, 12, 'Non'),
(27, 12, 'Oui');

-- --------------------------------------------------------

--
-- Structure de la table `resultats`
--

CREATE TABLE `resultats` (
  `id` int(11) NOT NULL,
  `idquestion` int(11) NOT NULL,
  `idreponse` int(11) NOT NULL,
  `iduser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `resultats`
--

INSERT INTO `resultats` (`id`, `idquestion`, `idreponse`, `iduser`) VALUES
(1, 10, 21, 1),
(2, 10, 20, 1),
(3, 10, 19, 1),
(4, 10, 18, 1),
(5, 11, 23, 1),
(6, 11, 22, 1),
(7, 10, 21, 2),
(8, 1, 1, 1),
(9, 1, 2, 2),
(10, 1, 2, 2),
(11, 1, 1, 3),
(12, 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `sexe` int(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  `age` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `username`, `sexe`, `email`, `age`) VALUES
(1, 'zdzqdqzd', 0, '', 0);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `questionnaire`
--
ALTER TABLE `questionnaire`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `reponses`
--
ALTER TABLE `reponses`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `resultats`
--
ALTER TABLE `resultats`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `questionnaire`
--
ALTER TABLE `questionnaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `reponses`
--
ALTER TABLE `reponses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT pour la table `resultats`
--
ALTER TABLE `resultats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
