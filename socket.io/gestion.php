<?php 
  
  require_once '../includes/config.php';

  if (!(isset($_SESSION['admin']))) {
    header('Location: connexion.php');
  }

  require_once 'generation_stats.php';

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Emballe Moi | Gestion</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="profile.php"><b>Emballe </b>Moi</a>
  </div>
              <?php

              if (isset($_POST['activerSpectacle'])) {

                $req = $odb -> query("UPDATE `config` SET actif = 1");

                echo '<div class="alert alert-success fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Success!</strong> Vous avez activé le spectacle !</div>';
              }

              if (isset($_POST['fermerSpectacle'])) {

                $req = $odb -> query("UPDATE `config` SET actif = 0");

                echo '<div class="alert alert-danger fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Success!</strong> Vous avez fermé le spectacle !</div>';
              }

              if (isset($_POST['remiseAZero'])) {

                $req = $odb -> query("DELETE FROM users");

                $deleteresultat = $odb->prepare('DELETE FROM resultats');
                $deleteresultat->execute();

                $dossier_traite = "imagesstats/";
               
                $repertoire = opendir($dossier_traite); // On définit le répertoire dans lequel on souhaite travailler.
                 
                while (false !== ($fichier = readdir($repertoire))) // On lit chaque fichier du répertoire dans la boucle.
                {
                $chemin = $dossier_traite."/".$fichier; // On définit le chemin du fichier à effacer.
                 
                // Si le fichier n'est pas un répertoire…
                if ($fichier != ".." AND $fichier != "." AND !is_dir($fichier))
                       {
                          unlink($chemin); // On efface.
                       }
                }
                closedir($repertoire); // Ne pas oublier de fermer le dossier ***EN DEHORS de la boucle*** ! Ce qui évitera à PHP beaucoup de calculs et des problèmes liés à l'ouverture du dossier.

                $req = $odb -> prepare('UPDATE questions SET visible = 0 WHERE visible = 1');
                $req -> execute();

                $tempsActuel = time();

                $req = $odb -> prepare('UPDATE questions SET visible = 1, temps=:temps WHERE id = 1');
                $req -> execute(array('temps' => $tempsActuel));

                echo '<div class="alert alert-warning fade in"><button type="button" class="close close-sm" data-dismiss="alert"><i class="fa fa-times"></i></button><strong>Success!</strong>Base de donnée remise à zéro !</div>';


              }

              if (isset($_POST['remiseAZeroUsers'])) {

                $req = $odb -> prepare("DELETE FROM users");
                $req -> execute();

              }

              if (isset($_POST['remiseAZeroStats'])) {

                $deleteresultat = $odb->prepare('DELETE FROM resultats');
                $deleteresultat->execute();

                $dossier_traite = "imagesstats/";
               
                $repertoire = opendir($dossier_traite); // On définit le répertoire dans lequel on souhaite travailler.
                 
                while (false !== ($fichier = readdir($repertoire))) // On lit chaque fichier du répertoire dans la boucle.
                {
                $chemin = $dossier_traite."/".$fichier; // On définit le chemin du fichier à effacer.
                 
                // Si le fichier n'est pas un répertoire…
                if ($fichier != ".." AND $fichier != "." AND !is_dir($fichier))
                       {
                          unlink($chemin); // On efface.
                       }
                }
                closedir($repertoire); // Ne pas oublier de fermer le dossier ***EN DEHORS de la boucle*** ! Ce qui évitera à PHP beaucoup de calculs et des problèmes liés à l'ouverture du dossier.

              }

            ?>
  <div class="register-box-body">
    <?php

      $query = $odb->prepare("SELECT `actif` FROM `config`");
      $query->execute();
      $actif = $query -> fetchColumn(0);

    ?>
    <p class="login-box-msg">Etat du spectacle : <?php if ($actif == 1) echo '<img src="../dist/img/power_on.png">'; else echo '<img src="../dist/img/power_off.png">'; ?></p>
    <form action="" method="post">

      <div class="col-lg">    
          <button type="submit" class="btn btn-success btn-block btn-flat" name="activerSpectacle">Activer le spectacle</button>
          <button type="submit" class="btn btn-danger btn-block btn-flat" name="fermerSpectacle">Fermer le spectacle</button>
          <button type="submit" class="btn btn-primary btn-block btn-flat" name="remiseAZero" onclick="return confirm('Êtes-vous sur ?')">Remise a zéro générale</button>
          <button type="submit" class="btn btn-primary btn-block btn-flat" name="remiseAZeroUsers" onclick="return confirm('Êtes-vous sur ?')">Remise a zéro utilisateurs</button>
          <button type="submit" class="btn btn-primary btn-block btn-flat" name="remiseAZeroStats" onclick="return confirm('Êtes-vous sur ?')">Remise a zéro Stats</button>
          <a href="gestion_messages.php" class="btn btn-warning btn-block btn-flat">Gestion Messages</a>
          <a href="modif_messages.php" class="btn btn-warning btn-block btn-flat">Modifier Messages</a>
          <a href="gestion_pages.php" class="btn btn-warning btn-block btn-flat">Gestion Pages</a>
          <a href="gestion_questions.php" class="btn btn-warning btn-block btn-flat">Gestion Questions</a>
          <a href="vibrations.php" class="btn btn-warning btn-block btn-flat">Gestion Vibrations</a>
          <a href="allstats.php" class="btn btn-danger btn-block btn-flat">Statistiques</a>
      </div>
    </form>
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>

<script type="text/javascript">
  var elems = document.getElementsByClassName('confirmation');
  var confirmIt = function (e) {
      if (!confirm('Êtes-vous sur ?')) e.preventDefault();
  };
  for (var i = 0, l = elems.length; i < l; i++) {
      elems[i].addEventListener('click', confirmIt, false);
  }
</script>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
