<?php 
  
  require_once '../includes/config.php';

  // On récupère le status du spectacle
  $query = $odb->prepare("SELECT `actif` FROM `config`");
  $query->execute();
  $actif = $query -> fetchColumn(0);

  // Si le spectacle n'est pas "ouvert", on redirige l'utilisateur vers la page d'attente
  if ($actif['actif'] == 0) {
    header('Location: attente.php');
  }

  // Si le profil n'a pas été complété, alors on redirige l'utilisateur vers la page de profil
  if (!isset($_SESSION['sexe'])) {
    header('Location: profil.php');
  }

  // On récupère le sexe pour parcourir les fichiers images
  if($_SESSION['sexe'] == 0)
    $sexe = "M";

  else
    $sexe = "F";

  // On ajoute un visage de base à l'arrivée sur la page
  if (!(isset($_SESSION['visage']))) {
    $_SESSION['visage'] = 'Visage_1.png';
  }
  
  // On récupère en $get l'element et on le stocke en session
  if(isset($_GET['visage']))
  {
    $visage = htmlspecialchars($_GET['visage']);
    $_SESSION['visage'] = $visage;
  }

  if(isset($_GET['cheveux']))
  {
    $cheveux = htmlspecialchars($_GET['cheveux']);
    $_SESSION['cheveux'] = $cheveux;
  }

  if(isset($_GET['yeux']))
  {
    $yeux = htmlspecialchars($_GET['yeux']);
    $_SESSION['yeux'] = $yeux;
  }

  if(isset($_GET['nez']))
  {
    $nez = htmlspecialchars($_GET['nez']);
    $_SESSION['nez'] = $nez;
  }

  if(isset($_GET['bouche']))
  {
    $bouche = htmlspecialchars($_GET['bouche']);
    $_SESSION['bouche'] = $bouche;
  }


  // Si l'accessoire selectionné est null, on vide la session accessoire
  if (isset($_GET['accessoire'])) {
    if ($_GET['accessoire' == 'null']) {
      $_SESSION['accessoire'] = '';
    }
    else
    {
      $accessoire = htmlspecialchars($_GET['accessoire']);
      $_SESSION['accessoire'] = $accessoire;
    }
  }

  // Si le bouton pour valider l'avatar est pressé, on créer une session avatarok
  if (isset($_POST['validerAvatar'])) {
    $_SESSION['avatarok'] = 'avatarok';
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Emballe Moi | Attente</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">
  <!-- Jquery Lib -->
  <script src="../js/jquery-3.3.1.min.js"></script>
  <!-- Socket.io Lib -->
  <script src="../socket.io/socket.io.js"></script>

  <style>
  #parent {
     position: relative;
  }
  img.superpose {
     position: absolute;
     top: 25px;
     left: 30px;
  }
  #img_1 {
     z-index: 10;
  }
  </style>

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href="../index2.html"><b>Emballe </b>Moi</a>
  </div>

  <div class="register-box-body" style="background-color: white;">
    <?php

    // Si la session avatarok existe, on affiche un message de remerciements et on affiche l'avatar de l'utilisateur
    if (isset($_SESSION['avatarok'])) {

    ?>
      <p class="login-box-msg">Ton avatar a bien été validé !</p>
      <div id="parent" class="square" style="background: #898b8e;">
              <img src="generation_avatar.php" class="img-responsive">
            </div>';
      <p class="login-box-msg"><br>Merci de patienter...</p>
    <?php
    }
    else
    {
    ?>
    <p class="login-box-msg">Laisse place à ton <font color="red">imagination</font> et créer l'avatar que tu souhaites !
 
  <span id="timer">
    <script type="text/javascript">// countDown();</script>
  </span>

  <div class="row text-center">
    <div class="col-sm-4">
      <u>Elements</u>
      <br /><br />

      <button data-toggle="collapse" class="btn btn-info" data-target="#visages">Visages</button>

      <!-- Visages -->
      <div id="visages" class="collapse">
        <br />
        <?php
          //nom du répertoire à lister
          $nom_repertoire = "../dist/img/Visage" . $sexe;
       
          //on ouvre un pointeur sur le repertoire
          $pointeur = opendir($nom_repertoire);
       
          //pour chaque fichier et dossier
          while ($fichier = readdir($pointeur))
          {
              //on ne traite pas les . et ..
              if(($fichier != '.') && ($fichier != '..') && ($fichier != '.DS_Store'))
              {
                  //si c'est un dossier, on le lit
                  if (is_dir($nom_repertoire.'/'.$fichier))
                  {
                      // On fait rien 
                  }

                  else
                  {
                      //c'est un fichier, on l'affiche
                      if(preg_match("#$extension#", "'.$fichier.'"))
                      {
                        echo '<a href="avatar.php?visage=' . $fichier . '">';
                        echo '<img src="' . $nom_repertoire . '/' . $fichier . '" class="img-responsive" />';
                        echo '</a><br />';
                      }
                  }
              }
          }
        ?>
      </div>

      <br><br>

      <button data-toggle="collapse" class="btn btn-info" data-target="#cheveux">Cheveux</button>

      <!-- Cheveux -->
      <div id="cheveux" class="collapse">
        <br />
        <?php
          //nom du répertoire à lister
          $nom_repertoire = "../dist/img/Cheveux" . $sexe;
       
          //on ouvre un pointeur sur le repertoire
          $pointeur = opendir($nom_repertoire);

          //pour chaque fichier et dossier
          while ($fichier = readdir($pointeur))
          {

              //on ne traite pas les . et ..
              if(($fichier != '.') && ($fichier != '..') && ($fichier != '.DS_Store'))
              {
                  //si c'est un dossier, on le lit
                  if (is_dir($nom_repertoire.'/'.$fichier))
                  {
                      // On fait rien 
                  }

                  else
                  {
                      //c'est un fichier, on l'affiche
                      if(preg_match("#$extension#", "'.$fichier.'"))
                      {
                        echo '<a href="avatar.php?cheveux=' . $fichier . '">';
                        echo '<img src="' . $nom_repertoire . '/' . $fichier . '" class="img-responsive img-rounded" />';
                        echo '</a><br />';
                      }
                  }
              }
          }
        ?>
      </div>

      <br><br>
      
      <button data-toggle="collapse" class="btn btn-info" data-target="#yeux">Yeux</button>

      <!-- Cheveux -->
      <div id="yeux" class="collapse">
        <br />
        <?php
          //nom du répertoire à lister
          $nom_repertoire = "../dist/img/Yeux" . $sexe;
       
          //on ouvre un pointeur sur le repertoire
          $pointeur = opendir($nom_repertoire);
       
          //pour chaque fichier et dossier
          while ($fichier = readdir($pointeur))
          {
              //on ne traite pas les . et ..
              if(($fichier != '.') && ($fichier != '..') && ($fichier != '.DS_Store'))
              {
                  //si c'est un dossier, on le lit
                  if (is_dir($nom_repertoire.'/'.$fichier))
                  {
                      // On fait rien 
                  }

                  else
                  {
                      //c'est un fichier, on l'affiche
                      if(preg_match("#$extension#", "'.$fichier.'"))
                      {
                        echo '<a href="avatar.php?yeux=' . $fichier . '">';
                        echo '<img src="' . $nom_repertoire . '/' . $fichier . '" class="img-responsive img-rounded" />';
                        echo '</a><br />';
                      }
                  }
              }
          }
        ?>
      </div>

      <br><br>
      
      <button data-toggle="collapse" class="btn btn-info" data-target="#nez">Nez</button>

      <!-- Cheveux -->
      <div id="nez" class="collapse">
        <br />
        <?php
          //nom du répertoire à lister
          $nom_repertoire = "../dist/img/Nez" . $sexe;
       
          //on ouvre un pointeur sur le repertoire
          $pointeur = opendir($nom_repertoire);
       
          //pour chaque fichier et dossier
          while ($fichier = readdir($pointeur))
          {
              //on ne traite pas les . et ..
              if(($fichier != '.') && ($fichier != '..') && ($fichier != '.DS_Store'))
              {
                  //si c'est un dossier, on le lit
                  if (is_dir($nom_repertoire.'/'.$fichier))
                  {
                      // On fait rien 
                  }

                  else
                  {
                      //c'est un fichier, on l'affiche
                      if(preg_match("#$extension#", "'.$fichier.'"))
                      {
                        echo '<a href="avatar.php?nez=' . $fichier . '">';
                        echo '<img src="' . $nom_repertoire . '/' . $fichier . '" class="img-responsive img-rounded" />';
                        echo '</a><br />';
                      }
                  }
              }
          }
        ?>
      </div>

      <br><br>
      
      <button data-toggle="collapse" class="btn btn-info" data-target="#bouche">Bouche</button>

      <!-- Cheveux -->
      <div id="bouche" class="collapse">
        <br />
        <?php
          //nom du répertoire à lister
          $nom_repertoire = "../dist/img/Bouche" . $sexe;
       
          //on ouvre un pointeur sur le repertoire
          $pointeur = opendir($nom_repertoire);
       
          //pour chaque fichier et dossier
          while ($fichier = readdir($pointeur))
          {
              //on ne traite pas les . et ..
              if(($fichier != '.') && ($fichier != '..') && ($fichier != '.DS_Store'))
              {
                  //si c'est un dossier, on le lit
                  if (is_dir($nom_repertoire.'/'.$fichier))
                  {
                      // On fait rien 
                  }

                  else
                  {
                      //c'est un fichier, on l'affiche
                      if(preg_match("#$extension#", "'.$fichier.'"))
                      {
                        echo '<a href="avatar.php?bouche=' . $fichier . '">';
                        echo '<img src="' . $nom_repertoire . '/' . $fichier . '" class="img-responsive img-rounded" />';
                        echo '</a><br />';
                      }
                  }
              }
          }
        ?>
      </div>

      <br><br>

      <button data-toggle="collapse" class="btn btn-info" data-target="#accessoire">Accessoires</button>

      <!-- Cheveux -->
      <div id="accessoire" class="collapse">
        <br />
        <a href="avatar.php?accessoire=null"><img src="../dist/img/Autres/eraser.png"></a>
        <?php
          //nom du répertoire à lister
          $nom_repertoire = "../dist/img/Accessoires" . $sexe;
       
          //on ouvre un pointeur sur le repertoire
          $pointeur = opendir($nom_repertoire);
       
          //pour chaque fichier et dossier
          while ($fichier = readdir($pointeur))
          {
              //on ne traite pas les . et ..
              if(($fichier != '.') && ($fichier != '..') && ($fichier != '.DS_Store'))
              {
                  //si c'est un dossier, on le lit
                  if (is_dir($nom_repertoire.'/'.$fichier))
                  {
                      // On fait rien 
                  }

                  else
                  {
                      //c'est un fichier, on l'affiche
                      if(preg_match("#$extension#", "'.$fichier.'"))
                      {
                        echo '<a href="avatar.php?accessoire=' . $fichier . '">';
                        echo '<img src="' . $nom_repertoire . '/' . $fichier . '" class="img-responsive img-rounded" />';
                        echo '</a><br />';
                      }
                  }
              }
          }
        ?>
      </div>
    </div>

    <div class="col-sm-8">
      <u>Avatar</u>
      <br />

      <div id="parent" class="square" style="background: #898b8e;">
        <img src="generation_avatar.php" class="img-responsive">
      </div>

      <br><br>
      <form method="post" action="">
      <!-- On fait apparaître un bouton de validation que si les cheveux, yeux, nez, bouche sont completés -->
      <?php if (isset($_SESSION['cheveux']) && isset($_SESSION['yeux']) && isset($_SESSION['nez']) && isset($_SESSION['bouche']))
      {
        echo '<button type="submit" class="btn btn-info btn-flat btn-block" name="validerAvatar">Valider mon avatar !</button>';
      }
      ?>
      </form>

    </div>
  </div>
</div>
<?php

}

?>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<script src="../js/changement.js"></script>
</body>
</html>
